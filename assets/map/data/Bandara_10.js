var json_Bandara_10 = {
"type": "FeatureCollection",
"name": "Bandara_10",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "NMBDR": "Abdurrahman Saleh", "NMKAB": "Kab. Malang", "NMPROP": "JAWA TIMUR" }, "geometry": { "type": "Point", "coordinates": [ 112.71445277799999, -7.926375 ] } },
{ "type": "Feature", "properties": { "NMBDR": "Juanda", "NMKAB": "Kab. Sidoarjo", "NMPROP": "JAWA TIMUR" }, "geometry": { "type": "Point", "coordinates": [ 112.78638888899999, -7.38083333299994 ] } },
{ "type": "Feature", "properties": { "NMBDR": "Iswahyudi (Madiun)", "NMKAB": "Kab. Magetan", "NMPROP": "JAWA TIMUR" }, "geometry": { "type": "Point", "coordinates": [ 111.434166667, -7.61583333299996 ] } },
{ "type": "Feature", "properties": { "NMBDR": "Pasuruan (Raci)", "NMKAB": "Kab. Pasuruan", "NMPROP": "JAWA TIMUR" }, "geometry": { "type": "Point", "coordinates": [ 112.845277778, -7.62305555599994 ] } },
{ "type": "Feature", "properties": { "NMBDR": "Cepu (Ngioram)", "NMKAB": "Kab. Blora", "NMPROP": "JAWA TENGAH" }, "geometry": { "type": "Point", "coordinates": [ 111.547777778, -7.19361111099994 ] } },
{ "type": "Feature", "properties": { "NMBDR": "Saur (Kangean)", "NMKAB": "Kab. Sumenep", "NMPROP": "JAWA TIMUR" }, "geometry": { "type": "Point", "coordinates": [ 115.65, -7.05 ] } },
{ "type": "Feature", "properties": { "NMBDR": "Truno Joyo\/Sumenep", "NMKAB": "Kab. Sumenep", "NMPROP": "JAWA TIMUR" }, "geometry": { "type": "Point", "coordinates": [ 113.937244444, -7.07025833399996 ] } },
{ "type": "Feature", "properties": { "NMBDR": "Blimbingsari", "NMKAB": "Kab. Banyuwangi", "NMPROP": "JAWA TIMUR" }, "geometry": { "type": "Point", "coordinates": [ 114.358333333, -8.313888888999941 ] } },
{ "type": "Feature", "properties": { "NMBDR": "Pagerungan", "NMKAB": "Kab. Sumenep", "NMPROP": "JAWA TIMUR" }, "geometry": { "type": "Point", "coordinates": [ 115.9325, -6.95666666699992 ] } }
]
}
