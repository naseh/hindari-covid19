var json_Pelabuhan_9 = {
"type": "FeatureCollection",
"name": "Pelabuhan_9",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "REMARK": "Pelabuhan Antar Pulau" }, "geometry": { "type": "Point", "coordinates": [ 113.802609256, -7.127769801 ] } },
{ "type": "Feature", "properties": { "REMARK": "Pelabuhan Antar Pulau" }, "geometry": { "type": "Point", "coordinates": [ 114.401076104000012, -8.143737253999983 ] } },
{ "type": "Feature", "properties": { "REMARK": "Pelabuhan Antar Pulau" }, "geometry": { "type": "Point", "coordinates": [ 112.960737539999954, -6.888446252 ] } },
{ "type": "Feature", "properties": { "REMARK": "Pelabuhan Antar Pulau" }, "geometry": { "type": "Point", "coordinates": [ 114.402435770000011, -8.134001080000031 ] } },
{ "type": "Feature", "properties": { "REMARK": "Pelabuhan Antar Pulau" }, "geometry": { "type": "Point", "coordinates": [ 112.72733311699993, -7.174804350999985 ] } },
{ "type": "Feature", "properties": { "REMARK": "Pelabuhan Antar Pulau" }, "geometry": { "type": "Point", "coordinates": [ 113.934035992999966, -7.693113763999983 ] } },
{ "type": "Feature", "properties": { "REMARK": "Pelabuhan Antar Pulau" }, "geometry": { "type": "Point", "coordinates": [ 115.705974188, -7.011393498000027 ] } },
{ "type": "Feature", "properties": { "REMARK": "Pelabuhan Antar Pulau" }, "geometry": { "type": "Point", "coordinates": [ 115.703909221000018, -7.007853074000025 ] } },
{ "type": "Feature", "properties": { "REMARK": "Pelabuhan Antar Pulau" }, "geometry": { "type": "Point", "coordinates": [ 115.228893829999947, -6.84302103300003 ] } },
{ "type": "Feature", "properties": { "REMARK": "Pelabuhan Antar Pulau" }, "geometry": { "type": "Point", "coordinates": [ 114.100335147, -6.980407775999979 ] } },
{ "type": "Feature", "properties": { "REMARK": "Pelabuhan Antar Pulau" }, "geometry": { "type": "Point", "coordinates": [ 113.944312680000067, -7.057404528000011 ] } },
{ "type": "Feature", "properties": { "REMARK": "Pelabuhan Antar Pulau" }, "geometry": { "type": "Point", "coordinates": [ 113.79940149399999, -7.213189938000025 ] } },
{ "type": "Feature", "properties": { "REMARK": "Pelabuhan Antar Pulau" }, "geometry": { "type": "Point", "coordinates": [ 114.40027866399997, -8.128020115000027 ] } },
{ "type": "Feature", "properties": { "REMARK": "Pelabuhan Antar Pulau" }, "geometry": { "type": "Point", "coordinates": [ 113.942782497000053, -7.060225296999985 ] } },
{ "type": "Feature", "properties": { "REMARK": "Pelabuhan Antar Pulau" }, "geometry": { "type": "Point", "coordinates": [ 112.722474372999955, -7.175939821 ] } },
{ "type": "Feature", "properties": { "REMARK": "Pelabuhan Antar Pulau" }, "geometry": { "type": "Point", "coordinates": [ 114.281881876000057, -7.079634885000035 ] } },
{ "type": "Feature", "properties": { "REMARK": "Pelabuhan Antar Pulau" }, "geometry": { "type": "Point", "coordinates": [ 113.786086657000041, -7.21071417000002 ] } },
{ "type": "Feature", "properties": { "REMARK": "Pelabuhan Antar Pulau" }, "geometry": { "type": "Point", "coordinates": [ 113.946314252999954, -7.043023694999989 ] } }
]
}
