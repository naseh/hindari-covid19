-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 09, 2020 at 12:49 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simerona`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_kotakab`
--

CREATE TABLE `tb_kotakab` (
  `id` int(11) NOT NULL,
  `nama_kota` varchar(150) NOT NULL,
  `odp` int(11) DEFAULT NULL,
  `pdp` int(11) DEFAULT NULL,
  `positif_corona` int(11) DEFAULT NULL,
  `otg` int(11) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kotakab`
--

INSERT INTO `tb_kotakab` (`id`, `nama_kota`, `odp`, `pdp`, `positif_corona`, `otg`, `jumlah`, `timestamp`) VALUES
(1, 'KABUPATEN BANGKALAN', 400, 3, 0, 8, 411, '2020-04-08 19:12:58'),
(2, 'KABUPATEN BANYUWANGI', 487, 5, 1, 62, 555, '2020-04-08 19:12:58'),
(3, 'KABUPATEN BLITAR', 551, 7, 1, 16, 575, '2020-04-08 19:12:58'),
(4, 'KABUPATEN BOJONEGORO', 107, 2, 0, 48, 157, '2020-04-08 19:12:58'),
(5, 'KABUPATEN BONDOWOSO', 799, 1, 1, 3, 804, '2020-04-08 19:12:58'),
(6, 'KABUPATEN GRESIK', 850, 69, 5, 60, 984, '2020-04-08 19:12:58'),
(7, 'KABUPATEN JEMBER', 714, 15, 2, 102, 833, '2020-04-08 19:12:58'),
(8, 'KABUPATEN JOMBANG', 377, 6, 2, 54, 439, '2020-04-08 19:12:58'),
(9, 'KABUPATEN KEDIRI', 338, 17, 7, 189, 551, '2020-04-08 19:12:58'),
(10, 'KOTA BATU', 108, 7, 1, 29, 145, '2020-04-08 19:12:58'),
(11, 'KOTA BLITAR', 143, 1, 1, 13, 158, '2020-04-08 19:12:58'),
(12, 'KOTA KEDIRI', 143, 5, 1, 47, 196, '2020-04-08 19:12:58'),
(13, 'KOTA MADIUN', 46, 8, 0, 3, 54, '2020-04-08 19:25:44'),
(14, 'KOTA MALANG', 369, 57, 8, 153, 587, '2020-04-08 19:12:58'),
(15, 'KOTA MOJOKERTO', 184, 6, 0, 1, 191, '2020-04-08 19:12:58'),
(16, 'KOTA PASURUAN', 53, 0, 0, 2, 53, '2020-04-08 19:25:44'),
(17, 'KOTA PROBOLINGGO', 180, 3, 0, 35, 183, '2020-04-08 19:25:44'),
(18, 'KOTA SURABAYA', 1056, 416, 84, 409, 1965, '2020-04-08 19:12:58'),
(19, 'KABUPATEN LAMONGAN', 218, 110, 13, 279, 620, '2020-04-08 19:12:58'),
(20, 'KABUPATEN LUMAJANG', 221, 18, 3, 90, 332, '2020-04-08 19:12:58'),
(21, 'KABUPATEN MADIUN', 172, 8, 1, 236, 417, '2020-04-08 19:12:58'),
(22, 'KABUPATEN MAGETAN', 112, 15, 9, 1, 137, '2020-04-08 19:12:58'),
(23, 'KABUPATEN MALANG', 176, 61, 10, 29, 276, '2020-04-08 19:12:58'),
(24, 'KABUPATEN MOJOKERTO', 215, 34, 0, 0, 249, '2020-04-08 19:12:58'),
(25, 'KABUPATEN NGANJUK', 36, 16, 7, 286, 345, '2020-04-08 19:12:58'),
(26, 'KABUPATEN NGAWI', 211, 7, 0, 5, 223, '2020-04-08 19:12:58'),
(27, 'KABUPATEN PACITAN', 411, 5, 0, 5, 421, '2020-04-08 19:12:58'),
(28, 'KABUPATEN PAMEKASAN', 197, 2, 2, 7510, 7711, '2020-04-08 19:12:58'),
(29, 'KABUPATEN PASURUAN', 33, 29, 0, 15, 77, '2020-04-08 19:12:58'),
(30, 'KABUPATEN PONOROGO', 298, 15, 3, 27, 343, '2020-04-08 19:12:58'),
(31, 'KABUPATEN PROBOLINGGO', 180, 3, 0, 35, 218, '2020-04-08 19:12:58'),
(32, 'KABUPATEN SAMPANG', 253, 0, 0, 5, 258, '2020-04-08 19:12:58'),
(33, 'KABUPATEN SIDOARJO', 347, 98, 18, 471, 934, '2020-04-08 19:12:58'),
(34, 'KABUPATEN SITUBONDO', 224, 21, 8, 2655, 2908, '2020-04-08 19:12:58'),
(35, 'KABUPATEN SUMENEP', 113, 0, 0, 13, 126, '2020-04-08 19:12:58'),
(36, 'KABUPATEN TRENGGALEK', 606, 8, 1, 7, 622, '2020-04-08 19:12:58'),
(37, 'KABUPATEN TUBAN', 432, 7, 2, 8, 449, '2020-04-08 19:12:58'),
(38, 'KABUPATEN TULUNGAGUNG', 856, 93, 5, 70, 1024, '2020-04-08 19:12:58');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `password`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_kotakab`
--
ALTER TABLE `tb_kotakab`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_kotakab`
--
ALTER TABLE `tb_kotakab`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
