<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

    Class Mod_tabel extends CI_Model
    {

		public function sumpdp(){
			$query = "select sum(pdp) as jumlah_pdp from tb_kotakab";
			$result = $this->db->query($query);
			return $result->row()->jumlah_pdp;
		}

		public function sumodp(){
			$query = "select sum(odp) as jumlah_odp from tb_kotakab";
			$result = $this->db->query($query);
			return $result->row()->jumlah_odp;
		}

		public function sumpositif(){
			$query = "select sum(positif_corona) as jumlah_positif from tb_kotakab";
			$result = $this->db->query($query);
			return $result->row()->jumlah_positif;
		}

		public function sumotg(){
			$query = "select sum(otg) as jumlah_otg from tb_kotakab";
			$result = $this->db->query($query);
			return $result->row()->jumlah_otg;
		}
	// 	private function angka($angka)
	// 	{	
	// 		return strrev(implode('.',str_split(strrev(strval($angka)),3)));
	// 	}
    //    public function tabelpropinsi()
    //    {
    //         $query = "SELECT tbp.id_propinsi,tbp.provinsi, CASE WHEN count(kp.id_propinsi) = 0 THEN 'Tidak Tersedia' ELSE 'Tersedia' END as status from test tbp left join keterangan_propinsi kp on tbp.id_propinsi = kp.id_propinsi group by tbp.provinsi ORDER BY `tbp`.`id_propinsi` ASC";
    //         $result = $this->db->query($query);
    //         return $result->result_array();
    //    }   

	//    public function tabelkabkot()
	//    {
	// 	   $query = "SELECT tbp.id_kabkot,tbp.kabkot, CASE WHEN count(kp.id_kab_kot) = 0 THEN 'Tidak Tersedia' ELSE 'Tersedia' END as status from test tbp left join keterangan_kab_kot kp on tbp.id_kabkot = kp.id_kab_kot group by tbp.kabkot";
	// 	   $result = $this->db->query($query);
	// 	   return $result->result_array();
	//    }

    //    public function insertprop($data)
    //    {
    //         $this->db->insert('keterangan_propinsi',$data);
    //    }

	//    public function insertkab_kot($data)
    //    {
    //         $this->db->insert('keterangan_kab_kot',$data);
    //    } 
	   
	//    public function deleteprop($id)
    //    {
    //         $this->db->where('id_propinsi', $id);
	// 		$this->db->delete('Keterangan_propinsi');
    //    }

	//    public function deletekabkot($id)
    //    {
    //         $this->db->where('id_kab_kot', $id);
	// 		$this->db->delete('keterangan_kab_kot');
    //    }

	//    //baru
	//    public function tampiltabelpro($id_propinsi)
    //    {
    //         $query = "SELECT `id_keterangan`, p.`nama_propinsi`, `tahun`, `populasi`, `jml_penduduk_laki_laki`, `jml_penduduk_perempuan`, `penduduk_di_bawah_umur`, `jumah_upah_minimum_provinsi`, `agama_islam`, `agama_kristen`, `agama_katolik`, `agama_hindu`, `agama_buddha`, `lain_lain`, `jdu_pertanian_kehutanan_dan_perikanan`, `jdu_pertambangan_dan_penggalian`, `jdu_industri_pengolahan`, `jdu_pengadaan_listrik_dan_gas`, `jdu_pengadaan_air_pengelolaan_sampah_limbah_dan_daur_ulang`, `jdu_konstruksi`, `jdu_perdagangan_besar_dan_eceran_reparasi_mobil_dan_sepeda_motor`, `jdu_transportasi_danPergudangan`, `jdu_penyediaan_akomodasi_makan_minum`, `jdu_informasi_dan_komunkasi`, `jdu_jasa_keuangan_dan_asuransi`, `jdu_real_estate`, `jdu_jasa_perusahaan`, `jdu_administrasi_pemerintahan_dan_jaminan_sosial`, `jdu_jasa_pendidikan`, `jdu_jasa_lainnya`, `jumlah_perusahaan_bumn`, `hewan_endemik`, `pternak_sapi`, `pternak_ayam`, `pternak_babi`, `inflasi_deflasi` FROM `keterangan_propinsi` k join tb_propinsi p on k.id_propinsi=p.id_propinsi  WHERE k.id_propinsi=$id_propinsi";
	// 		//k.id_propinsi=$id_propinsi
	// 		//UPPER(p.nama_propinsi)='$id_propinsi'";
    //         $result = $this->db->query($query);
    //         if ($result->num_rows() == 0){
	// 			return "Data Tidak Tersedia";
	// 		}
	// 		  $tabel=  ' <div class="table-responsive">          
  	// 		<table class="table text-right" border="1">
    // 		<thead>
	// 			<tr>
	// 			<th rowspan="2">Tahun</th>
	// 			<th rowspan="2">Populasi</th>
	// 			<th rowspan="2">Laki-Laki</th>
	// 			<th rowspan="2">Perempuan</th>
	// 			<th rowspan="2">Penduduk Usia Di Bawah Umur</th>
	// 			<th rowspan="2">Jumlah Upah Minimum Provinsi</th>
	// 			<th colspan="6"><center>Agama</center></th>
	// 			<th colspan="16">Jalur Distribusi Usaha (Produk <br> Domestik Regional Bruto) <br> Harga Berlaku(Juta/Milyar)</th>
	// 			<th rowspan="2">Jumlah Perusahaan BUMN</th>
	// 			<th rowspan="2">Hewan Endemik</th>
	// 			<th colspan="3"><center>Produksi Ternak</center></th>
	// 			<th rowspan="2">INFLASI/ DEFLASI dlm %</th>
	// 			</tr>
	// 			<tr>
	// 			<th>Islam</th>
	// 			<th>Kristen</th>
	// 			<th>Katholik</th>
	// 			<th>Hindu</th>
	// 			<th>Buddha</th>
	// 			<th>Lain-Lain</th>
	// 			<th>Pertanian Kehutanan dan Perikanan</th>
	// 			<th>Pertambangan dan Penggalian</th>
	// 			<th>Industri Pengolahan</th>
	// 			<th>Pengadaan Listrik dan Gas</th>
	// 			<th>Pengadaan Air, Pengelolaan Sampah, Limbah dan Daur Ulang</th>
	// 			<th>Konstruksi</th>
	// 			<th>Perdagangan Besar dan Eceran, Reparasi Mobil dan Sepeda Motor</th>
	// 			<th>Transportasi dan Pergudangan</th>
	// 			<th>Penyediaan Akomodasi dan Makan Minum</th>
	// 			<th>Informasi dan Komunikasi</th>
	// 			<th>Jasa Keuangan dan Asuransi</th>
	// 			<th>Real Estate</th>
	// 			<th>Jasa Perusahaan</th>
	// 			<th>Administrasi Pemerintahan, Pertahanan dan Jaminan Sosial</th>
	// 			<th>Jasa Pendidikan</th>
	// 			<th>Jasa Lainnya</th>
	// 			<th>Sapi</th>
	// 			<th>Ayam</th>
	// 			<th>Babi</th>
	// 			</tr>
	// 			</thead>
	// 			<tbody>
	// 			';
   
	// 		foreach($result->result_array() as $data){
	// 			$tabel= $tabel.
	// 			"<tr><td>".$data['tahun']."</td>  <td>".$this->angka($data['populasi'])."</td>  <td>".$this->angka($data['jml_penduduk_laki_laki'])."</td>  <td>".$this->angka($data['jml_penduduk_perempuan'])."</td>  <td>".$this->angka($data['penduduk_di_bawah_umur'])."</td>  <td> Rp.".$this->angka($data['jumah_upah_minimum_provinsi']).",00</td>  <td>".$this->angka($data['agama_islam'])."</td>  <td>".$this->angka($data['agama_kristen'])."</td>  <td>".$this->angka($data['agama_katolik'])."</td>  <td>".$this->angka($data['agama_hindu'])."</td>  <td>".$this->angka($data['agama_buddha'])."</td>  <td>".$this->angka($data['lain_lain'])."</td>  <td>".$this->angka($data['jdu_pertanian_kehutanan_dan_perikanan'])."</td>  <td>".$this->angka($data['jdu_pertambangan_dan_penggalian'])."</td>  <td>".$this->angka($data['jdu_industri_pengolahan'])."</td>  <td>".$this->angka($data['jdu_pengadaan_listrik_dan_gas'])."</td>  <td>".$this->angka($data['jdu_pengadaan_air_pengelolaan_sampah_limbah_dan_daur_ulang'])."</td>  <td>".$this->angka($data['jdu_konstruksi'])."</td>  <td>".$this->angka($data['jdu_perdagangan_besar_dan_eceran_reparasi_mobil_dan_sepeda_motor'])."</td>  <td>".$this->angka($data['jdu_transportasi_danPergudangan'])."</td>  <td>".$this->angka($data['jdu_penyediaan_akomodasi_makan_minum'])."</td>  <td>".$this->angka($data['jdu_informasi_dan_komunkasi'])."</td>  <td>".$this->angka($data['jdu_jasa_keuangan_dan_asuransi'])."</td>  <td>".$this->angka($data['jdu_real_estate'])."</td>  <td>".$this->angka($data['jdu_jasa_perusahaan'])."</td>  <td>".$this->angka($data['jdu_administrasi_pemerintahan_dan_jaminan_sosial'])."</td>  <td>".$this->angka($data['jdu_jasa_pendidikan'])."</td>  <td>".$this->angka($data['jdu_jasa_lainnya'])."</td>  <td>".$this->angka($data['jumlah_perusahaan_bumn'])."</td>  <td>".$this->angka($data['hewan_endemik'])."</td>  <td>".$this->angka($data['pternak_sapi'])."</td>  <td>".$this->angka($data['pternak_ayam'])."</td>  <td>".$this->angka($data['pternak_babi'])."</td>  <td>".$this->angka($data['inflasi_deflasi'])."</td></tr>"; 
	// 		}
	// 		 $tabel= $tabel. '</tbody></table></div>';
	// 		 return $tabel;
    //    }
	// 	//baru
	// 	public function tampiltabelkab($id_kab){
    //         $query = "SELECT `id_keterangan`, p.`kabkot`, `tahun`, `populasi`, `jml_penduduk_laki_laki`, `jml_penduduk_perempuan`, `penduduk_di_bawah_umur`, `jumah_upah_minimum_kab_kot`, `agama_islam`, `agama_kristen`, `agama_katolik`, `agama_hindu`, `agama_buddha`, `lain_lain`, `jdu_pertanian_kehutanan_dan_perikanan`, `jdu_pertambangan_dan_penggalian`, `jdu_industri_pengolahan`, `jdu_pengadaan_listrik_dan_gas`, `jdu_pengadaan_air_pengelolaan_sampah_limbah_dan_daur_ulang`, `jdu_konstruksi`, `jdu_perdagangan_besar_dan_eceran_reparasi_mobil_dan_sepeda_motor`, `jdu_transportasi_danPergudangan`, `jdu_penyediaan_akomodasi_makan_minum`, `jdu_informasi_dan_komunkasi`, `jdu_jasa_keuangan_dan_asuransi`, `jdu_real_estate`, `jdu_jasa_perusahaan`, `jdu_administrasi_pemerintahan_dan_jaminan_sosial`, `jdu_jasa_pendidikan`, `jdu_jasa_lainnya`, `jumlah_perusahaan_bumn`, `hewan_endemik`, `pternak_sapi`, `pternak_ayam`, `pternak_babi`, `inflasi_deflasi` FROM `keterangan_kab_kot` k join test p on k.id_kab_kot=p.id_kabkot WHERE k.id_kab_kot=$id_kab";
	// 		//k.id_propinsi=$id_propinsi
	// 		//UPPER(p.nama_propinsi)='$id_propinsi'";
    //         $result = $this->db->query($query);
    //         if ($result->num_rows() == 0){
	// 			return "Data Tidak Tersedia";
	// 		}
	// 		 $tabel=  ' <div class="table-responsive">          
  	// 		<table class="table text-right" border="1">
    // 		<thead>
	// 			<tr>
	// 			<th rowspan="2">Tahun</th>
	// 			<th rowspan="2">Populasi</th>
	// 			<th rowspan="2">Laki-Laki</th>
	// 			<th rowspan="2">Perempuan</th>
	// 			<th rowspan="2">Penduduk Usia Di Bawah Umur</th>
	// 			<th rowspan="2">Jumlah Upah Minimum Kota/Kabupaten</th>
	// 			<th colspan="6"><center>Agama</center></th>
	// 			<th colspan="16"><center>Jalur Distribusi Usaha (Produk <br> Domestik Regional Bruto) <br> Harga Berlaku(Juta/Milyar)</center></th>
	// 			<th rowspan="2">Jumlah Perusahaan BUMN</th>
	// 			<th rowspan="2">Hewan Endemik</th>
	// 			<th colspan="3"><center>Produksi Ternak</center></th>
	// 			<th rowspan="2">INFLASI/ DEFLASI dlm %</th>
	// 			</tr>
	// 			<tr>
	// 			<th>Islam</th>
	// 			<th>Kristen</th>
	// 			<th>Katholik</th>
	// 			<th>Hindu</th>
	// 			<th>Buddha</th>
	// 			<th>Lain-Lain</th>
	// 			<th>Pertanian Kehutanan dan Perikanan</th>
	// 			<th>Pertambangan dan Penggalian</th>
	// 			<th>Industri Pengolahan</th>
	// 			<th>Pengadaan Listrik dan Gas</th>
	// 			<th>Pengadaan Air, Pengelolaan Sampah, Limbah dan Daur Ulang</th>
	// 			<th>Konstruksi</th>
	// 			<th>Perdagangan Besar dan Eceran, Reparasi Mobil dan Sepeda Motor</th>
	// 			<th>Transportasi dan Pergudangan</th>
	// 			<th>Penyediaan Akomodasi dan Makan Minum</th>
	// 			<th>Informasi dan Komunikasi</th>
	// 			<th>Jasa Keuangan dan Asuransi</th>
	// 			<th>Real Estate</th>
	// 			<th>Jasa Perusahaan</th>
	// 			<th>Administrasi Pemerintahan, Pertahanan dan Jaminan Sosial</th>
	// 			<th>Jasa Pendidikan</th>
	// 			<th>Jasa Lainnya</th>
	// 			<th>Sapi</th>
	// 			<th>Ayam</th>
	// 			<th>Babi</th>
	// 			</tr>
	// 			</thead>
	// 			<tbody>
	// 			';
   
	// 		foreach($result->result_array() as $data){
	// 			$tabel= $tabel.
	// 			"<tr><td>".$data['tahun']."</td>  <td>".$this->angka($data['populasi'])."</td>  <td>".$this->angka($data['jml_penduduk_laki_laki'])."</td>  <td>".$this->angka($data['jml_penduduk_perempuan'])."</td>  <td>".$this->angka($data['penduduk_di_bawah_umur'])."</td>  <td> Rp.".$this->angka($data['jumah_upah_minimum_kab_kot']).",00</td>  <td>".$this->angka($data['agama_islam'])."</td>  <td>".$this->angka($data['agama_kristen'])."</td>  <td>".$this->angka($data['agama_katolik'])."</td>  <td>".$this->angka($data['agama_hindu'])."</td>  <td>".$this->angka($data['agama_buddha'])."</td>  <td>".$this->angka($data['lain_lain'])."</td>  <td>".$this->angka($data['jdu_pertanian_kehutanan_dan_perikanan'])."</td>  <td>".$this->angka($data['jdu_pertambangan_dan_penggalian'])."</td>  <td>".$this->angka($data['jdu_industri_pengolahan'])."</td>  <td>".$this->angka($data['jdu_pengadaan_listrik_dan_gas'])."</td>  <td>".$this->angka($data['jdu_pengadaan_air_pengelolaan_sampah_limbah_dan_daur_ulang'])."</td>  <td>".$this->angka($data['jdu_konstruksi'])."</td>  <td>".$this->angka($data['jdu_perdagangan_besar_dan_eceran_reparasi_mobil_dan_sepeda_motor'])."</td>  <td>".$this->angka($data['jdu_transportasi_danPergudangan'])."</td>  <td>".$this->angka($data['jdu_penyediaan_akomodasi_makan_minum'])."</td>  <td>".$this->angka($data['jdu_informasi_dan_komunkasi'])."</td>  <td>".$this->angka($data['jdu_jasa_keuangan_dan_asuransi'])."</td>  <td>".$this->angka($data['jdu_real_estate'])."</td>  <td>".$this->angka($data['jdu_jasa_perusahaan'])."</td>  <td>".$this->angka($data['jdu_administrasi_pemerintahan_dan_jaminan_sosial'])."</td>  <td>".$this->angka($data['jdu_jasa_pendidikan'])."</td>  <td>".$this->angka($data['jdu_jasa_lainnya'])."</td>  <td>".$this->angka($data['jumlah_perusahaan_bumn'])."</td>  <td>".$this->angka($data['hewan_endemik'])."</td>  <td>".$this->angka($data['pternak_sapi'])."</td>  <td>".$this->angka($data['pternak_ayam'])."</td>  <td>".$this->angka($data['pternak_babi'])."</td>  <td>".$this->angka($data['inflasi_deflasi'])."</td></tr>";  
	// 		}
	// 		 $tabel= $tabel. '</tbody></table></div>';
	// 		 return $tabel;
    //    }

	//    public function jmlprop()
	//    {
	// 	   $query = "SELECT COUNT(*) AS jmlprop FROM tb_propinsi";
	// 	   $result = $this->db->query($query);
	// 	   return $result->row()->jmlprop;
	//    }

	//    public function jmlkabkot()
	//    {
	// 	   $query = "SELECT COUNT(*) AS jmlkabkot FROM tb_kab_kot";
	// 	   $result = $this->db->query($query);
	// 	   return $result->row()->jmlkabkot;
	//    }

	//    function simpanpin($data){
	// 		$this->db->insert('pin', $data);
	// 		return $data['isi']; 
	//    }
	   
	//    function tampilpin()
	// 	{
	// 	 $query = "SELECT * FROM `pin`";
	// 	 $result = $this->db->query($query);
	// 	 return $result->result_array();
	// 	}

	//    public function deletepin($id)
    //    {
    //         $this->db->where('id', $id);
	// 		$this->db->delete('pin');
    //    }
	// 	//baru kabupaten
    //    public function chartpenduduk($id=439)
    //    {
    //    		$query = "SELECT tahun,populasi, `jml_penduduk_laki_laki`, `jml_penduduk_perempuan`, `penduduk_di_bawah_umur`, `jumah_upah_minimum_kab_kot` from keterangan_kab_kot where `id_kab_kot`='$id'  order by tahun";
    //    		$result = $this->db->query($query);
	// 		$data=array();
	// 		$temp=array();
	// 		if ($result->num_rows() == 0){
	// 			$data['tahun_penduduk'][0]="Gagal";
	// 			return $data;
	// 		}
	// 		foreach( $result->result_array() as $hasil){
	// 			$data['tahun_penduduk'][]=(string)$hasil['tahun'];
	// 			$temp['populasi'][]=(int)$hasil['populasi'];
	// 			$temp['jml_penduduk_laki_laki'][]=(int)$hasil['jml_penduduk_laki_laki'];
	// 			$temp['jml_penduduk_perempuan'][]=(int)$hasil['jml_penduduk_perempuan'];
	// 			$temp['penduduk_di_bawah_umur'][]=(int)$hasil['penduduk_di_bawah_umur'];
	// 			$temp['jumah_upah_minimum_kab_kot'][]=(int)$hasil['jumah_upah_minimum_kab_kot'];
	// 		}
			
	// 		$data['penduduk'][]=array('name'=>'Populasi','data'=>$temp['populasi']);
	// 		$data['penduduk'][]=array('name'=>'Jumlah Penduduk Laki-Laki','data'=> $temp['jml_penduduk_laki_laki']);
	// 		$data['penduduk'][]=array('name'=>'Jumlah Penduduk Perempuan','data'=> $temp['jml_penduduk_perempuan']);
	// 		$data['penduduk'][]=array('name'=>'Jumlah Penduduk Dibawah Umur','data'=> $temp['penduduk_di_bawah_umur']);
	// 		$data['penduduk'][]=array('name'=>'Jumlah Upah Minimum','data'=> $temp['jumah_upah_minimum_kab_kot']);
			
    //    		return $data; 
    //    }
	   
	// 	public function chartagama($id=439)
    //    {
    //    		$query = "SELECT tahun, `agama_islam`, `agama_kristen`, `agama_katolik`, `agama_hindu`, `agama_buddha`, `lain_lain` from keterangan_kab_kot where `id_kab_kot`='$id'  order by tahun";
    //    		$result = $this->db->query($query);
	// 		$data=array();
	// 		$temp=array();
			
	// 		foreach( $result->result_array() as $hasil){
	// 			$data['tahun_agama'][]=(string)$hasil['tahun'];
	// 			$temp['agama_islam'][]=(int)$hasil['agama_islam'];
	// 			$temp['agama_kristen'][]=(int)$hasil['agama_kristen'];
	// 			$temp['agama_katolik'][]=(int)$hasil['agama_katolik'];
	// 			$temp['agama_hindu'][]=(int)$hasil['agama_hindu'];
	// 			$temp['agama_buddha'][]=(int)$hasil['agama_buddha'];
	// 			$temp['lain_lain'][]=(int)$hasil['lain_lain'];
	// 		}
			
	// 		$data['agama'][]=array('name'=>'Agama Islam','data'=>$temp['agama_islam']);
	// 		$data['agama'][]=array('name'=>'Agama Kristen','data'=> $temp['agama_kristen']);
	// 		$data['agama'][]=array('name'=>'Agama Katolik','data'=> $temp['agama_katolik']);
	// 		$data['agama'][]=array('name'=>'Agama Hindu','data'=> $temp['agama_hindu']);
	// 		$data['agama'][]=array('name'=>'Agama Buddha','data'=> $temp['agama_buddha']);
	// 		$data['agama'][]=array('name'=>'Lain-lain','data'=> $temp['lain_lain']);
			
    //    		return $data; 
    //    }
	   
	//    public function charthasilbumi($id=439)
    //    {
    //    		$query = "SELECT tahun, `jdu_pertanian_kehutanan_dan_perikanan`, `jdu_pertambangan_dan_penggalian`, `jdu_industri_pengolahan`, `jdu_pengadaan_listrik_dan_gas`, `jdu_pengadaan_air_pengelolaan_sampah_limbah_dan_daur_ulang` from keterangan_kab_kot where `id_kab_kot`='$id'  order by tahun";
    //    		$result = $this->db->query($query);
	// 		$data=array();
	// 		$temp=array();
			
	// 		foreach( $result->result_array() as $hasil){
	// 			$data['tahun_hasilbumi'][]=(string)$hasil['tahun'];
	// 			$temp['jdu_pertanian_kehutanan_dan_perikanan'][]=(int)$hasil['jdu_pertanian_kehutanan_dan_perikanan'];
	// 			$temp['jdu_pertambangan_dan_penggalian'][]=(int)$hasil['jdu_pertambangan_dan_penggalian'];
	// 			$temp['jdu_industri_pengolahan'][]=(int)$hasil['jdu_industri_pengolahan'];
	// 			$temp['jdu_pengadaan_listrik_dan_gas'][]=(int)$hasil['jdu_pengadaan_listrik_dan_gas'];
	// 			$temp['jdu_pengadaan_air_pengelolaan_sampah_limbah_dan_daur_ulang'][]=(int)$hasil['jdu_pengadaan_air_pengelolaan_sampah_limbah_dan_daur_ulang'];
				
	// 		}
			
	// 		$data['hasilbumi'][]=array('name'=>'Pertanian dan Kehutanan','data'=>$temp['jdu_pertanian_kehutanan_dan_perikanan']);
	// 		$data['hasilbumi'][]=array('name'=>'Pertambangan dan Penggalian','data'=> $temp['jdu_pertambangan_dan_penggalian']);
	// 		$data['hasilbumi'][]=array('name'=>'Industri Pengolahan','data'=> $temp['jdu_industri_pengolahan']);
	// 		$data['hasilbumi'][]=array('name'=>'Listrik dan Gas','data'=> $temp['jdu_pengadaan_listrik_dan_gas']);
	// 		$data['hasilbumi'][]=array('name'=>'Pengadaan Air Pengelolaan Limbah dan Daur Ulang','data'=> $temp['jdu_pengadaan_air_pengelolaan_sampah_limbah_dan_daur_ulang']);
			
    //    		return $data; 
    //    }
	   
	//    public function chartakomodasi($id=439)
    //    {
    //    		$query = "SELECT tahun, `jdu_konstruksi`, `jdu_perdagangan_besar_dan_eceran_reparasi_mobil_dan_sepeda_motor`, `jdu_transportasi_danPergudangan`, `jdu_penyediaan_akomodasi_makan_minum`, `jdu_informasi_dan_komunkasi` from keterangan_kab_kot where `id_kab_kot`='$id'  order by tahun";
    //    		$result = $this->db->query($query);
	// 		$data=array();
	// 		$temp=array();
			
	// 		foreach( $result->result_array() as $hasil){
	// 			$data['tahun_akomodasi'][]=(string)$hasil['tahun'];
	// 			$temp['jdu_konstruksi'][]=(int)$hasil['jdu_konstruksi'];
	// 			$temp['jdu_perdagangan_besar_dan_eceran_reparasi_mobil_dan_sepeda_motor'][]=(int)$hasil['jdu_perdagangan_besar_dan_eceran_reparasi_mobil_dan_sepeda_motor'];
	// 			$temp['jdu_transportasi_danPergudangan'][]=(int)$hasil['jdu_transportasi_danPergudangan'];
	// 			$temp['jdu_penyediaan_akomodasi_makan_minum'][]=(int)$hasil['jdu_penyediaan_akomodasi_makan_minum'];
	// 			$temp['jdu_informasi_dan_komunkasi'][]=(int)$hasil['jdu_informasi_dan_komunkasi'];
				
	// 		}
			
	// 		$data['akomodasi'][]=array('name'=>'Konstruksi','data'=>$temp['jdu_konstruksi']);
	// 		$data['akomodasi'][]=array('name'=>'Reparasi Mobil dan Sepeda Motor','data'=> $temp['jdu_perdagangan_besar_dan_eceran_reparasi_mobil_dan_sepeda_motor']);
	// 		$data['akomodasi'][]=array('name'=>'Transportasi dan Pergudangan','data'=> $temp['jdu_transportasi_danPergudangan']);
	// 		$data['akomodasi'][]=array('name'=>'Makan dan Minum','data'=> $temp['jdu_penyediaan_akomodasi_makan_minum']);
	// 		$data['akomodasi'][]=array('name'=>'Informasi dan Komunikasi','data'=> $temp['jdu_informasi_dan_komunkasi']);
			
    //    		return $data; 
    //    }
	   
	//    public function chartusaha($id=439)
    //    {
    //    		$query = "SELECT tahun, `jdu_jasa_keuangan_dan_asuransi`, `jdu_real_estate`, `jdu_jasa_perusahaan`, `jdu_administrasi_pemerintahan_dan_jaminan_sosial`, `jdu_jasa_pendidikan`, `jdu_jasa_lainnya`, `jumlah_perusahaan_bumn` from keterangan_kab_kot where `id_kab_kot`='$id'  order by tahun";
    //    		$result = $this->db->query($query);
	// 		$data=array();
	// 		$temp=array();
			
	// 		foreach( $result->result_array() as $hasil){
	// 			$data['tahun_usaha'][]=(string)$hasil['tahun'];
	// 			$temp['jdu_jasa_keuangan_dan_asuransi'][]=(int)$hasil['jdu_jasa_keuangan_dan_asuransi'];
	// 			$temp['jdu_real_estate'][]=(int)$hasil['jdu_real_estate'];
	// 			$temp['jdu_jasa_perusahaan'][]=(int)$hasil['jdu_jasa_perusahaan'];
	// 			$temp['jdu_administrasi_pemerintahan_dan_jaminan_sosial'][]=(int)$hasil['jdu_administrasi_pemerintahan_dan_jaminan_sosial'];
	// 			$temp['jdu_jasa_pendidikan'][]=(int)$hasil['jdu_jasa_pendidikan'];
	// 			$temp['jdu_jasa_lainnya'][]=(int)$hasil['jdu_jasa_lainnya'];
	// 			$temp['jumlah_perusahaan_bumn'][]=(int)$hasil['jumlah_perusahaan_bumn'];
				
	// 		}
			
	// 		$data['usaha'][]=array('name'=>'Keuangan dan Asuransi','data'=> $temp['jdu_jasa_keuangan_dan_asuransi']);
	// 		$data['usaha'][]=array('name'=>'Real Estate','data'=> $temp['jdu_real_estate']);
	// 		$data['usaha'][]=array('name'=>'Jasa Perusahaan','data'=> $temp['jdu_jasa_perusahaan']);
	// 		$data['usaha'][]=array('name'=>'Administrasi Pemerintah dan Jaminan Sosial','data'=> $temp['jdu_administrasi_pemerintahan_dan_jaminan_sosial']);
	// 		$data['usaha'][]=array('name'=>'Jasa Pendidikan','data'=> $temp['jdu_jasa_pendidikan']);
	// 		$data['usaha'][]=array('name'=>'Jasa Lainnya','data'=> $temp['jdu_jasa_lainnya']);
	// 		$data['usaha'][]=array('name'=>'Jumlah Perusahaan BUMN','data'=> $temp['jumlah_perusahaan_bumn']);
			
    //    		return $data; 
    //    }
	   
	//    public function charthewan($id=439)
    //    {
    //    		$query = "SELECT tahun, `hewan_endemik`, `pternak_sapi`, `pternak_ayam`, `pternak_babi` from keterangan_kab_kot where `id_kab_kot`='$id'  order by tahun";
    //    		$result = $this->db->query($query);
	// 		$data=array();
	// 		$temp=array();
			
	// 		foreach( $result->result_array() as $hasil){
	// 			$data['tahun_hewan'][]=(string)$hasil['tahun'];
	// 			$temp['hewan_endemik'][]=(int)$hasil['hewan_endemik'];
	// 			$temp['pternak_sapi'][]=(int)$hasil['pternak_sapi'];
	// 			$temp['pternak_ayam'][]=(int)$hasil['pternak_ayam'];
	// 			$temp['pternak_babi'][]=(int)$hasil['pternak_babi'];
				
	// 		}
			
	// 		$data['hewan'][]=array('name'=>'Hewan Endemik','data'=> $temp['hewan_endemik']);
	// 		$data['hewan'][]=array('name'=>'Sapi','data'=> $temp['pternak_sapi']);
	// 		$data['hewan'][]=array('name'=>'Ayam','data'=> $temp['pternak_ayam']);
	// 		$data['hewan'][]=array('name'=>'Babi','data'=> $temp['pternak_babi']);
			
    //    		return $data; 
    //    }
	    
	// 	public function chartinflasi($id=439)
    //    {
    //    		$query = "SELECT tahun, `inflasi_deflasi` from keterangan_kab_kot where `id_kab_kot`='$id'  order by tahun";
    //    		$result = $this->db->query($query);
	// 		$data=array();
	// 		$temp=array();
			
	// 		foreach( $result->result_array() as $hasil){
	// 			$data['tahun_inflasi'][]=(string)$hasil['tahun'];
	// 			$temp['inflasi_deflasi'][]=(int)$hasil['inflasi_deflasi'];
				
	// 		}
			
	// 		$data['inflasi'][]=array('name'=>'Inflasi/Deflasi','data'=> $temp['inflasi_deflasi']);
			
    //    		return $data; 
    //    }
	//    //baru prov
    //    public function chartpendudukprov($id=32)
    //    {
    //    		$query = "SELECT tahun,populasi, `jml_penduduk_laki_laki`, `jml_penduduk_perempuan`, `penduduk_di_bawah_umur`, `jumah_upah_minimum_provinsi` from keterangan_propinsi where `id_propinsi`='$id'  order by tahun";
    //    		$result = $this->db->query($query);
	// 		$data=array();
	// 		$temp=array();
	// 		if ($result->num_rows() == 0){
	// 			$data['tahun_penduduk'][0]="Gagal";
	// 			return $data;
	// 		}
	// 		foreach( $result->result_array() as $hasil){
	// 			$data['tahun_penduduk'][]=(string)$hasil['tahun'];
	// 			$temp['populasi'][]=(int)$hasil['populasi'];
	// 			$temp['jml_penduduk_laki_laki'][]=(int)$hasil['jml_penduduk_laki_laki'];
	// 			$temp['jml_penduduk_perempuan'][]=(int)$hasil['jml_penduduk_perempuan'];
	// 			$temp['penduduk_di_bawah_umur'][]=(int)$hasil['penduduk_di_bawah_umur'];
	// 			$temp['jumah_upah_minimum_provinsi'][]=(int)$hasil['jumah_upah_minimum_provinsi'];
	// 		}
			
	// 		$data['penduduk'][]=array('name'=>'Populasi','data'=>$temp['populasi']);
	// 		$data['penduduk'][]=array('name'=>'Jumlah Penduduk Laki-Laki','data'=> $temp['jml_penduduk_laki_laki']);
	// 		$data['penduduk'][]=array('name'=>'Jumlah Penduduk Perempuan','data'=> $temp['jml_penduduk_perempuan']);
	// 		$data['penduduk'][]=array('name'=>'Jumlah Penduduk Dibawah Umur','data'=> $temp['penduduk_di_bawah_umur']);
	// 		$data['penduduk'][]=array('name'=>'Jumlah Upah Minimum','data'=> $temp['jumah_upah_minimum_provinsi']);
			
    //    		return $data; 
    //    }
	   
	// 	public function chartagamaprov($id=32)
    //    {
    //    		$query = "SELECT tahun, `agama_islam`, `agama_kristen`, `agama_katolik`, `agama_hindu`, `agama_buddha`, `lain_lain` from keterangan_propinsi where `id_propinsi`='$id'  order by tahun";
    //    		$result = $this->db->query($query);
	// 		$data=array();
	// 		$temp=array();
			
	// 		foreach( $result->result_array() as $hasil){
	// 			$data['tahun_agama'][]=(string)$hasil['tahun'];
	// 			$temp['agama_islam'][]=(int)$hasil['agama_islam'];
	// 			$temp['agama_kristen'][]=(int)$hasil['agama_kristen'];
	// 			$temp['agama_katolik'][]=(int)$hasil['agama_katolik'];
	// 			$temp['agama_hindu'][]=(int)$hasil['agama_hindu'];
	// 			$temp['agama_buddha'][]=(int)$hasil['agama_buddha'];
	// 			$temp['lain_lain'][]=(int)$hasil['lain_lain'];
	// 		}
			
	// 		$data['agama'][]=array('name'=>'Agama Islam','data'=>$temp['agama_islam']);
	// 		$data['agama'][]=array('name'=>'Agama Kristen','data'=> $temp['agama_kristen']);
	// 		$data['agama'][]=array('name'=>'Agama Katolik','data'=> $temp['agama_katolik']);
	// 		$data['agama'][]=array('name'=>'Agama Hindu','data'=> $temp['agama_hindu']);
	// 		$data['agama'][]=array('name'=>'Agama Buddha','data'=> $temp['agama_buddha']);
	// 		$data['agama'][]=array('name'=>'Lain-lain','data'=> $temp['lain_lain']);
			
    //    		return $data; 
    //    }
	   
	//    public function charthasilbumiprov($id=32)
    //    {
    //    		$query = "SELECT tahun, `jdu_pertanian_kehutanan_dan_perikanan`, `jdu_pertambangan_dan_penggalian`, `jdu_industri_pengolahan`, `jdu_pengadaan_listrik_dan_gas`, `jdu_pengadaan_air_pengelolaan_sampah_limbah_dan_daur_ulang` from keterangan_propinsi where `id_propinsi`='$id'  order by tahun";
    //    		$result = $this->db->query($query);
	// 		$data=array();
	// 		$temp=array();
			
	// 		foreach( $result->result_array() as $hasil){
	// 			$data['tahun_hasilbumi'][]=(string)$hasil['tahun'];
	// 			$temp['jdu_pertanian_kehutanan_dan_perikanan'][]=(int)$hasil['jdu_pertanian_kehutanan_dan_perikanan'];
	// 			$temp['jdu_pertambangan_dan_penggalian'][]=(int)$hasil['jdu_pertambangan_dan_penggalian'];
	// 			$temp['jdu_industri_pengolahan'][]=(int)$hasil['jdu_industri_pengolahan'];
	// 			$temp['jdu_pengadaan_listrik_dan_gas'][]=(int)$hasil['jdu_pengadaan_listrik_dan_gas'];
	// 			$temp['jdu_pengadaan_air_pengelolaan_sampah_limbah_dan_daur_ulang'][]=(int)$hasil['jdu_pengadaan_air_pengelolaan_sampah_limbah_dan_daur_ulang'];
				
	// 		}
			
	// 		$data['hasilbumi'][]=array('name'=>'Pertanian dan Kehutanan','data'=>$temp['jdu_pertanian_kehutanan_dan_perikanan']);
	// 		$data['hasilbumi'][]=array('name'=>'Pertambangan dan Penggalian','data'=> $temp['jdu_pertambangan_dan_penggalian']);
	// 		$data['hasilbumi'][]=array('name'=>'Industri Pengolahan','data'=> $temp['jdu_industri_pengolahan']);
	// 		$data['hasilbumi'][]=array('name'=>'Listrik dan Gas','data'=> $temp['jdu_pengadaan_listrik_dan_gas']);
	// 		$data['hasilbumi'][]=array('name'=>'Pengadaan Air Pengelolaan Limbah dan Daur Ulang','data'=> $temp['jdu_pengadaan_air_pengelolaan_sampah_limbah_dan_daur_ulang']);
			
    //    		return $data; 
    //    }
	   
	//    public function chartakomodasiprov($id=32)
    //    {
    //    		$query = "SELECT tahun, `jdu_konstruksi`, `jdu_perdagangan_besar_dan_eceran_reparasi_mobil_dan_sepeda_motor`, `jdu_transportasi_danPergudangan`, `jdu_penyediaan_akomodasi_makan_minum`, `jdu_informasi_dan_komunkasi` from keterangan_propinsi where `id_propinsi`='$id'  order by tahun";
    //    		$result = $this->db->query($query);
	// 		$data=array();
	// 		$temp=array();
			
	// 		foreach( $result->result_array() as $hasil){
	// 			$data['tahun_akomodasi'][]=(string)$hasil['tahun'];
	// 			$temp['jdu_konstruksi'][]=(int)$hasil['jdu_konstruksi'];
	// 			$temp['jdu_perdagangan_besar_dan_eceran_reparasi_mobil_dan_sepeda_motor'][]=(int)$hasil['jdu_perdagangan_besar_dan_eceran_reparasi_mobil_dan_sepeda_motor'];
	// 			$temp['jdu_transportasi_danPergudangan'][]=(int)$hasil['jdu_transportasi_danPergudangan'];
	// 			$temp['jdu_penyediaan_akomodasi_makan_minum'][]=(int)$hasil['jdu_penyediaan_akomodasi_makan_minum'];
	// 			$temp['jdu_informasi_dan_komunkasi'][]=(int)$hasil['jdu_informasi_dan_komunkasi'];
				
	// 		}
			
	// 		$data['akomodasi'][]=array('name'=>'Konstruksi','data'=>$temp['jdu_konstruksi']);
	// 		$data['akomodasi'][]=array('name'=>'Reparasi Mobil dan Sepeda Motor','data'=> $temp['jdu_perdagangan_besar_dan_eceran_reparasi_mobil_dan_sepeda_motor']);
	// 		$data['akomodasi'][]=array('name'=>'Transportasi dan Pergudangan','data'=> $temp['jdu_transportasi_danPergudangan']);
	// 		$data['akomodasi'][]=array('name'=>'Makan dan Minum','data'=> $temp['jdu_penyediaan_akomodasi_makan_minum']);
	// 		$data['akomodasi'][]=array('name'=>'Informasi dan Komunikasi','data'=> $temp['jdu_informasi_dan_komunkasi']);
			
    //    		return $data; 
    //    }
	   
	//    public function chartusahaprov($id=32)
    //    {
    //    		$query = "SELECT tahun, `jdu_jasa_keuangan_dan_asuransi`, `jdu_real_estate`, `jdu_jasa_perusahaan`, `jdu_administrasi_pemerintahan_dan_jaminan_sosial`, `jdu_jasa_pendidikan`, `jdu_jasa_lainnya`, `jumlah_perusahaan_bumn` from keterangan_propinsi where `id_propinsi`='$id'  order by tahun";
    //    		$result = $this->db->query($query);
	// 		$data=array();
	// 		$temp=array();
			
	// 		foreach( $result->result_array() as $hasil){
	// 			$data['tahun_usaha'][]=(string)$hasil['tahun'];
	// 			$temp['jdu_jasa_keuangan_dan_asuransi'][]=(int)$hasil['jdu_jasa_keuangan_dan_asuransi'];
	// 			$temp['jdu_real_estate'][]=(int)$hasil['jdu_real_estate'];
	// 			$temp['jdu_jasa_perusahaan'][]=(int)$hasil['jdu_jasa_perusahaan'];
	// 			$temp['jdu_administrasi_pemerintahan_dan_jaminan_sosial'][]=(int)$hasil['jdu_administrasi_pemerintahan_dan_jaminan_sosial'];
	// 			$temp['jdu_jasa_pendidikan'][]=(int)$hasil['jdu_jasa_pendidikan'];
	// 			$temp['jdu_jasa_lainnya'][]=(int)$hasil['jdu_jasa_lainnya'];
	// 			$temp['jumlah_perusahaan_bumn'][]=(int)$hasil['jumlah_perusahaan_bumn'];
				
	// 		}
			
	// 		$data['usaha'][]=array('name'=>'Keuangan dan Asuransi','data'=> $temp['jdu_jasa_keuangan_dan_asuransi']);
	// 		$data['usaha'][]=array('name'=>'Real Estate','data'=> $temp['jdu_real_estate']);
	// 		$data['usaha'][]=array('name'=>'Jasa Perusahaan','data'=> $temp['jdu_jasa_perusahaan']);
	// 		$data['usaha'][]=array('name'=>'Administrasi Pemerintah dan Jaminan Sosial','data'=> $temp['jdu_administrasi_pemerintahan_dan_jaminan_sosial']);
	// 		$data['usaha'][]=array('name'=>'Jasa Pendidikan','data'=> $temp['jdu_jasa_pendidikan']);
	// 		$data['usaha'][]=array('name'=>'Jasa Lainnya','data'=> $temp['jdu_jasa_lainnya']);
	// 		$data['usaha'][]=array('name'=>'Jumlah Perusahaan BUMN','data'=> $temp['jumlah_perusahaan_bumn']);
			
    //    		return $data; 
    //    }
	   
	//    public function charthewanprov($id=32)
    //    {
    //    		$query = "SELECT tahun, `hewan_endemik`, `pternak_sapi`, `pternak_ayam`, `pternak_babi` from keterangan_propinsi where `id_propinsi`='$id'  order by tahun";
    //    		$result = $this->db->query($query);
	// 		$data=array();
	// 		$temp=array();
			
	// 		foreach( $result->result_array() as $hasil){
	// 			$data['tahun_hewan'][]=(string)$hasil['tahun'];
	// 			$temp['hewan_endemik'][]=(int)$hasil['hewan_endemik'];
	// 			$temp['pternak_sapi'][]=(int)$hasil['pternak_sapi'];
	// 			$temp['pternak_ayam'][]=(int)$hasil['pternak_ayam'];
	// 			$temp['pternak_babi'][]=(int)$hasil['pternak_babi'];
				
	// 		}
			
	// 		$data['hewan'][]=array('name'=>'Hewan Endemik','data'=> $temp['hewan_endemik']);
	// 		$data['hewan'][]=array('name'=>'Sapi','data'=> $temp['pternak_sapi']);
	// 		$data['hewan'][]=array('name'=>'Ayam','data'=> $temp['pternak_ayam']);
	// 		$data['hewan'][]=array('name'=>'Babi','data'=> $temp['pternak_babi']);
			
    //    		return $data; 
    //    }
	    
	// 	public function chartinflasiprov($id=32)
    //    {
    //    		$query = "SELECT tahun, `inflasi_deflasi` from keterangan_propinsi where `id_propinsi`='$id'  order by tahun";
    //    		$result = $this->db->query($query);
	// 		$data=array();
	// 		$temp=array();
			
	// 		foreach( $result->result_array() as $hasil){
	// 			$data['tahun_inflasi'][]=(string)$hasil['tahun'];
	// 			$temp['inflasi_deflasi'][]=(int)$hasil['inflasi_deflasi'];
				
	// 		}
			
	// 		$data['inflasi'][]=array('name'=>'Inflasi/Deflasi','data'=> $temp['inflasi_deflasi']);
			
    //    		return $data; 
    //    }
	}

?>