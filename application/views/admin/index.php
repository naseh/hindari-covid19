<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
  <meta name="author" content="GeeksLabs">
  <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
  <link rel="shortcut icon" href="img/favicon.png">

  <title>SIMERONA | Halaman Admin </title>

  <!-- Bootstrap CSS -->
  <link href="<?php echo base_url();?>assets/be/css/bootstrap.min.css" rel="stylesheet">
  <!-- bootstrap theme -->
  <link href="<?php echo base_url();?>assets/be/css/bootstrap-theme.css" rel="stylesheet">
  <!--external css-->
  <!-- font icon -->
  <link href="<?php echo base_url();?>assets/be/css/elegant-icons-style.css" rel="stylesheet" />
  <link href="<?php echo base_url();?>assets/be/css/font-awesome.min.css" rel="stylesheet" />
  <!-- full calendar css-->
  <link href="<?php echo base_url();?>assets/be/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
  <link href="<?php echo base_url();?>assets/be/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
  <!-- easy pie chart-->
  <link href="<?php echo base_url();?>assets/be/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen" />
  <!-- owl carousel -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/be/css/owl.carousel.css" type="text/css">
  <link href="<?php echo base_url();?>assets/be/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
  <!-- Custom styles -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/be/css/fullcalendar.css">
  <link href="<?php echo base_url();?>assets/be/css/widgets.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/be/css/style.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/be/css/style-responsive.css" rel="stylesheet" />
  <link href="<?php echo base_url();?>assets/be/css/xcharts.min.css" rel=" stylesheet">
  <link href="<?php echo base_url();?>assets/be/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
  
  <!-- map -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/map/css/leaflet.css">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/map/css/L.Control.Locate.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/map/css/qgis2web.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/map/css/Control.OSMGeocoder.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        script src="<?php echo base_url();?>assets/map/js/qgis2web_expressions.js"></script>
        <script src="<?php echo base_url();?>assets/map/js/leaflet.js"></script>
        <script src="<?php echo base_url();?>assets/map/js/L.Control.Locate.min.js"></script>
        <script src="<?php echo base_url();?>assets/map/js/multi-style-layer.js"></script>
        <script src="<?php echo base_url();?>assets/map/js/leaflet.rotatedMarker.js"></script>
        <script src="<?php echo base_url();?>assets/map/js/leaflet.pattern.js"></script>
        <script src="<?php echo base_url();?>assets/map/js/leaflet-hash.js"></script>
        <script src="<?php echo base_url();?>assets/map/js/Autolinker.min.js"></script>
        <script src="<?php echo base_url();?>assets/map/js/rbush.min.js"></script>
        <script src="<?php echo base_url();?>assets/map/js/labelgun.min.js"></script>
        <script src="<?php echo base_url();?>assets/map/js/labels.js"></script>
        <script src="<?php echo base_url();?>assets/map/js/Control.OSMGeocoder.js"></script>
        <script src="<?php echo base_url();?>assets/map/data/COVID19_1.js"></script>
        <script src="<?php echo base_url();?>assets/map/data/Jarakamandaribandarasejauh5km_2.js"></script>
        <script src="<?php echo base_url();?>assets/map/data/Jarakamandariterminalbussejauh4km_3.js"></script>
        <script src="<?php echo base_url();?>assets/map/data/Jarakamandaristasiunsejauh3km_4.js"></script>
        <script src="<?php echo base_url();?>assets/map/data/Jarakamandaripelabuhansejauh2km_5.js"></script>
        <script src="<?php echo base_url();?>assets/map/data/JalurKA_6.js"></script>
        <script src="<?php echo base_url();?>assets/map/data/StasiunKA_7.js"></script>
        <script src="<?php echo base_url();?>assets/map/data/TerminalBus_8.js"></script>
        <script src="<?php echo base_url();?>assets/map/data/Pelabuhan_9.js"></script>
        <script src="<?php echo base_url();?>assets/map/data/Bandara_10.js"></script>
        <style>
        #map {
            width: 100%;
            height: 541px;
        }		
		.footer {
		   left: 0;
		   bottom: 0;
		   width: 100%;
		   background-color: #9c0000;
		   color: white;
		   text-align: center;
		   margin-top:20px;
		}
		.table > thead > tr > th {
			vertical-align: middle;
		}
	</style>
        <!-- =======================================================
    Theme Name: NiceAdmin
    Theme URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>

<body>
  <!-- container section start -->
  <section id="container" class="">


    <header class="header dark-bg" style="background-color:red;">
      <div class="toggle-nav">
        <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
      </div>

      <!--logo start-->
      <a href="index.html" class="logo">SIME <span class="lite" style="color:white;">RONA</span></a>
      <!--logo end-->

      <div class="nav search-row" id="top_menu">
        <!--  search form start -->
        <ul class="nav top-menu">
          <li>
            <form class="navbar-form">
              <input class="form-control" placeholder="Search" type="text">
            </form>
          </li>
        </ul>
        <!--  search form end -->
      </div>

      <div class="top-nav notification-row">
        <!-- notificatoin dropdown start-->
        <ul class="nav pull-right top-menu">

          <!-- task notificatoin start -->
          
          <!-- task notificatoin end -->
          <!-- inbox notificatoin start-->
          
          <!-- inbox notificatoin end -->
          <!-- alert notification start-->
          
          <!-- alert notification end-->
          <!-- user login dropdown start-->
          <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="profile-ava">
                                <img alt="" src="<?php echo base_url();?>assets/be/img/admin-small.png">
                            </span>
                            <span class="username">Admin</span>
                            <b class="caret"></b>
                        </a>
            <ul class="dropdown-menu extended logout">
              <div class="log-arrow-up"></div>
              <li class="eborder-top">
                <a href="#"><i class="icon_profile"></i> My Profile</a>
              </li>
              <li>
                <a href="#"><i class="icon_mail_alt"></i> My Inbox</a>
              </li>
              <li>
                <a href="#"><i class="icon_clock_alt"></i> Timeline</a>
              </li>
              <li>
                <a href="#"><i class="icon_chat_alt"></i> Chats</a>
              </li>
              <li>
                <a href="<?php echo site_url('cont_login')?>"><i class="icon_key_alt"></i> Log Out</a>
              </li>
              <li>
                <a href="#"><i class="icon_key_alt"></i> Documentation</a>
              </li>
              <li>
                <a href="#"><i class="icon_key_alt"></i> Documentation</a>
              </li>
            </ul>
          </li>
          <!-- user login dropdown end -->
        </ul>
        <!-- notificatoin dropdown end-->
      </div>
    </header>
    <!--header end-->

    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu">
          <li class="active">
            <a class="" href="<?php echo site_url('cont_admin/index');?>">
                          <i class="icon_house_alt"></i>
                          <span>Dashboard</span>
                      </a>
          </li>
          <li>
            <a class="" href="<?php echo site_url('cont_umum/lihatpeta');?>">
                          <i class="icon_map"></i>
                          <span>Peta Zona Bahayai <br> Status</span>
                      </a>
          </li>
          <li>
            <a class="" href="<?php echo site_url('cont_umum/lihatpetademografi');?>">
                          <i class="icon_map"></i>
                          <span>Peta Demografi</span>

            </a>
          </li>
          <li>
            <a class="" href="<?php echo site_url('cont_umum/lihatpetalansia');?>">
                          <i class="icon_map"></i>
                          <span>Peta Lansia</span>

                      </a>
          </li>
         

        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-laptop"></i> Dashboard</h3>
            <ol class="breadcrumb">
              <li><i class="fa fa-home"></i><a href="index.html">Home</a></li>
              <li><i class="fa fa-laptop"></i>Dashboard</li>
            </ol>
          </div>
        </div>

        <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="info-box blue-bg">
                <h3><b>Pasien Dalam Pengawasan</b></h3>
               <div class="count"><?php echo $sumpdp?></div>
            </div>
            <!--/.info-box-->
          </div>
          <!--/.col-->

          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="info-box brown-bg">
            <h3><b>Orang Dalam Pengawasan</b></h3>
               <div class="count"><?php echo $sumodp?></div>
            </div>
            <!--/.info-box-->
          </div>
          <!--/.col-->

          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="info-box dark-bg">
            <h3><b>Positif Corona</b></h3>
               <div class="count"><?php echo $sumpositif?></div>
            </div>
            <!--/.info-box-->
          </div>
          <!--/.col-->

          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="info-box green-bg">
            <h3><b>Orang Tanpa Gejala</b></h3>
               <div class="count"><?php echo $sumotg?></div>
            </div>
            <!--/.info-box-->
          </div>
          <!--/.col-->

        </div>
        <!--/.row-->


        <!-- <div class="col-lg-6">
                              <section class="panel">
                                  <header class="panel-heading">
                                      Peta Zona Bahaya Covid
                                  </header>
                                  
                                  <div id="map" style="width:100%; height:83.4%; " class="leaflet html-widget html-widget-output"></div>
                                  
                              </section>
                          </div>

        </div> -->


        <!-- Today status end -->



        
        <!-- statics end -->




        <!-- project team & activity start -->
      
        <!-- project team & activity end -->

      </section>
      <div class="text-right">
        <div class="credits">
          <!--
            All the links in the footer should remain intact.
            You can delete the links only if you purchased the pro version.
            Licensing information: https://bootstrapmade.com/license/
            Purchase the pro version form: https://bootstrapmade.com/buy/?theme=NiceAdmin
          -->
          Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
        </div>
      </div>
    </section>
    <!--main content end-->
  </section>
  <!-- container section start -->

  <!-- javascripts -->
  <script src="<?php echo base_url();?>assets/be/js/jquery.js"></script>
  <script src="<?php echo base_url();?>assets/be/js/jquery-ui-1.10.4.min.js"></script>
  <script src="<?php echo base_url();?>assets/be/js/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/be/js/jquery-ui-1.9.2.custom.min.js"></script>
  <!-- bootstrap -->
  <script src="<?php echo base_url();?>assets/be/js/bootstrap.min.js"></script>
  <!-- nice scroll -->
  <script src="<?php echo base_url();?>assets/be/js/jquery.scrollTo.min.js"></script>
  <script src="<?php echo base_url();?>assets/be/js/jquery.nicescroll.js" type="text/javascript"></script>
  <!-- charts scripts -->
  <script src="<?php echo base_url();?>assets/be/assets/jquery-knob/js/jquery.knob.js"></script>
  <script src="<?php echo base_url();?>assets/be/js/jquery.sparkline.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/be/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
  <script src="<?php echo base_url();?>assets/be/js/owl.carousel.js"></script>
  <!-- jQuery full calendar -->
  <<script src="<?php echo base_url();?>assets/be/js/fullcalendar.min.js"></script>
    <!-- Full Google Calendar - Calendar -->
    <script src="<?php echo base_url();?>assets/be/assets/fullcalendar/fullcalendar/fullcalendar.js"></script>
    <!--script for this page only-->
    <script src="<?php echo base_url();?>assets/be/js/calendar-custom.js"></script>
    <script src="<?php echo base_url();?>assets/be/js/jquery.rateit.min.js"></script>
    <!-- custom select -->
    <script src="<?php echo base_url();?>assets/be/js/jquery.customSelect.min.js"></script>
    <script src="<?php echo base_url();?>assets/be/assets/chart-master/Chart.js"></script>

    <!--custome script for all page-->
    <script src="<?php echo base_url();?>assets/be/js/scripts.js"></script>
    <!-- custom script for this page-->
    <script src="<?php echo base_url();?>assets/be/js/sparkline-chart.js"></script>
    <script src="<?php echo base_url();?>assets/be/js/easy-pie-chart.js"></script>
    <script src="<?php echo base_url();?>assets/be/js/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?php echo base_url();?>assets/be/js/jquery-jvectormap-world-mill-en.js"></script>
    <script src="<?php echo base_url();?>assets/be/js/xcharts.min.js"></script>
    <script src="<?php echo base_url();?>assets/be/js/jquery.autosize.min.js"></script>
    <script src="<?php echo base_url();?>assets/be/js/jquery.placeholder.min.js"></script>
    <script src="<?php echo base_url();?>assets/be/js/gdp-data.js"></script>
    <script src="<?php echo base_url();?>assets/be/js/morris.min.js"></script>
    <script src="<?php echo base_url();?>assets/be/js/sparklines.js"></script>
    <script src="<?php echo base_url();?>assets/be/js/charts.js"></script>
    <script src="<?php echo base_url();?>assets/be/js/jquery.slimscroll.min.js"></script>
    
    
    
    <!-- <script>
      //knob
      $(function() {
        $(".knob").knob({
          'draw': function() {
            $(this.i).val(this.cv + '%')
          }
        })
      });

      //carousel
      $(document).ready(function() {
        $("#owl-slider").owlCarousel({
          navigation: true,
          slideSpeed: 300,
          paginationSpeed: 400,
          singleItem: true

        });
      });

      //custom select box

      $(function() {
        $('select.styled').customSelect();
      });

      /* ---------- Map ---------- */
      $(function() {
        $('#map').vectorMap({
          map: 'world_mill_en',
          series: {
            regions: [{
              values: gdpData,
              scale: ['#000', '#000'],
              normalizeFunction: 'polynomial'
            }]
          },
          backgroundColor: '#eef3f7',
          onLabelShow: function(e, el, code) {
            el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
          }
        });
      });
    </script> -->
    <script>
         var highlightLayer;
        function highlightFeature(e) {
            highlightLayer = e.target;

            if (e.target.feature.geometry.type === 'LineString') {
              highlightLayer.setStyle({
                color: '#ffff00',
              });
            } else {
              highlightLayer.setStyle({
                fillColor: '#ffff00',
                fillOpacity: 1
              });
            }
        }
        var map = L.map('map', {
            zoomControl:true, maxZoom:28, minZoom:5
        }).fitBounds([[-9.01915644205,111.018555023],[-6.38135354086,114.481601077]]);
        var hash = new L.Hash(map);
        map.attributionControl.addAttribution('<a href="https://github.com/tomchadwin/qgis2web" target="_blank">qgis2web</a>');
        L.control.locate().addTo(map);
        var bounds_group = new L.featureGroup([]);
        var basemap0 = L.tileLayer('http://a.tile.stamen.com/toner/{z}/{x}/{y}.png', {
            attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>,<a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Mapdata: &copy; <a href="http://openstreetmap.org">OpenStreetMap</a>contributors,<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
            maxZoom: 28
        });
        basemap0.addTo(map);
        function setBounds() {
        }
        var img_KepadatanPenduduk_0 = '<?php echo base_url();?>assets/map/data/KepadatanPenduduk_0.png';
        var img_bounds_KepadatanPenduduk_0 = [[-8.78047714445,110.8987198],[-6.4810327,116.270108689]];
        var overlay_KepadatanPenduduk_0 = new L.imageOverlay(img_KepadatanPenduduk_0, img_bounds_KepadatanPenduduk_0);
        bounds_group.addLayer(overlay_KepadatanPenduduk_0);
        map.addLayer(overlay_KepadatanPenduduk_0);
        function pop_COVID19_1(feature, layer) {
            layer.on({
                mouseout: function(e) {
                    for (i in e.target._eventParents) {
                        e.target._eventParents[i].resetStyle(e.target);
                    }
                },
                mouseover: highlightFeature,
            });
            var popupContent = '<table>\
                    <tr>\
                        <td colspan="2">' + (feature.properties['ADM1_EN'] !== null ? Autolinker.link(String(feature.properties['ADM1_EN'])) : '') + '</td>\
                    </tr>\
                    <tr>\
                        <td colspan="2">' + (feature.properties['ADM2_EN'] !== null ? Autolinker.link(String(feature.properties['ADM2_EN'])) : '') + '</td>\
                    </tr>\
                    <tr>\
                        <th scope="row">Positif</th>\
                        <td>' + (feature.properties['Positif'] !== null ? Autolinker.link(String(feature.properties['Positif'])) : '') + '</td>\
                    </tr>\
                    <tr>\
                        <th scope="row">PDP</th>\
                        <td>' + (feature.properties['PDP'] !== null ? Autolinker.link(String(feature.properties['PDP'])) : '') + '</td>\
                    </tr>\
                    <tr>\
                        <th scope="row">ODP</th>\
                        <td>' + (feature.properties['ODP'] !== null ? Autolinker.link(String(feature.properties['ODP'])) : '') + '</td>\
                    </tr>\
                </table>';
            layer.bindPopup(popupContent, {maxHeight: 400});
        }

        var legend = new L.Control({position: 'bottomleft'});
        legend.onAdd = function (map) {
          
          this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
          
          this.update();
          return this._div;
        };
        legend.update = function () {
          this._div.innerHTML = '<div class="card panel-right d-none d-md-block"><div class="card-body"><h3 class="card-title" style="color:black;">Hindari COVID-19</h3><p class="card-text text-justify">Peta ini memvisualisasikan zona bahaya<br> berdasarkan moda transportasi (kereta api,<br> pesawat, bus dan kapal laut).Data diperoleh dari<br> dinas perhubungan RI. Kemudian dibandingkan<br> dengan data kepadatan penduduk yang berasal<br> dari Office for the Coordination of Humanitarian<br> Affairs (OCHA) di bawah PBB.<br>Peta ini tidak dapat<br> digunakan sebagai landasan utama pencegahan,<br>hanya sebagai data pendukung.Diperlukan data<br> lain agar informasi lebih akurat.</p><footer class="blockquote-footer">Dikembangkan oleh : <cite title="Source Title">Fatwa Ramdani, Universitas Brawijaya</cite></footer><a href="https://www.unocha.org/" target="_blank">Sumber : Office For the Cordination of Humanitarian<br> Affairs</a></div></div>';
        };
        
        legend.addTo(map);


        function style_COVID19_1_0(feature) {
            switch(String(feature.properties['Positif'])) {
                case '0':
                    return {
                pane: 'pane_COVID19_1',
                opacity: 1,
                color: 'rgba(129,122,122,0.5)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 2.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(119,200,92,0.0)',
            }
                    break;
                case '1':
                    return {
                pane: 'pane_COVID19_1',
                opacity: 1,
                color: 'rgba(129,122,122,0.5)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 2.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(253,13,57,0.5)',
            }
                    break;
                case '3':
                    return {
                pane: 'pane_COVID19_1',
                opacity: 1,
                color: 'rgba(129,122,122,0.5)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 2.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(253,13,57,0.5)',
            }
                    break;
                case '4':
                    return {
                pane: 'pane_COVID19_1',
                opacity: 1,
                color: 'rgba(129,122,122,0.5)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 2.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(253,13,57,0.5)',
            }
                    break;
                case '29':
                    return {
                pane: 'pane_COVID19_1',
                opacity: 1,
                color: 'rgba(129,122,122,0.5)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 2.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(253,13,57,0.5)',
            }
                    break;
                default:
                    return {
                pane: 'pane_COVID19_1',
                opacity: 1,
                color: 'rgba(129,122,122,0.5)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 2.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(218,187,94,0.5)',
            }
                    break;
            }
        }
        map.createPane('pane_COVID19_1');
        map.getPane('pane_COVID19_1').style.zIndex = 401;
        map.getPane('pane_COVID19_1').style['mix-blend-mode'] = 'normal';
        var layer_COVID19_1 = new L.geoJson(json_COVID19_1, {
            attribution: '<a href=""></a>',
            pane: 'pane_COVID19_1',
            onEachFeature: pop_COVID19_1,
            style: style_COVID19_1_0,
        });
        bounds_group.addLayer(layer_COVID19_1);
        map.addLayer(layer_COVID19_1);
        function pop_Jarakamandaribandarasejauh5km_2(feature, layer) {
            layer.on({
                mouseout: function(e) {
                    for (i in e.target._eventParents) {
                        e.target._eventParents[i].resetStyle(e.target);
                    }
                },
                mouseover: highlightFeature,
            });
            var popupContent = '<table>\
                    <tr>\
                        <td colspan="2">' + (feature.properties['fid'] !== null ? Autolinker.link(String(feature.properties['fid'])) : '') + '</td>\
                    </tr>\
                    <tr>\
                        <td colspan="2">' + (feature.properties['NMBDR'] !== null ? Autolinker.link(String(feature.properties['NMBDR'])) : '') + '</td>\
                    </tr>\
                    <tr>\
                        <td colspan="2">' + (feature.properties['NMKAB'] !== null ? Autolinker.link(String(feature.properties['NMKAB'])) : '') + '</td>\
                    </tr>\
                    <tr>\
                        <td colspan="2">' + (feature.properties['NMPROP'] !== null ? Autolinker.link(String(feature.properties['NMPROP'])) : '') + '</td>\
                    </tr>\
                </table>';
            layer.bindPopup(popupContent, {maxHeight: 400});
        }

        function style_Jarakamandaribandarasejauh5km_2_0() {
            return {
                pane: 'pane_Jarakamandaribandarasejauh5km_2',
                opacity: 1,
                color: 'rgba(227,26,28,0.5)',
                dashArray: '10,5',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(253,13,57,0.5)',
            }
        }
        map.createPane('pane_Jarakamandaribandarasejauh5km_2');
        map.getPane('pane_Jarakamandaribandarasejauh5km_2').style.zIndex = 402;
        map.getPane('pane_Jarakamandaribandarasejauh5km_2').style['mix-blend-mode'] = 'normal';
        var layer_Jarakamandaribandarasejauh5km_2 = new L.geoJson(json_Jarakamandaribandarasejauh5km_2, {
            attribution: '<a href=""></a>',
            pane: 'pane_Jarakamandaribandarasejauh5km_2',
            onEachFeature: pop_Jarakamandaribandarasejauh5km_2,
            style: style_Jarakamandaribandarasejauh5km_2_0,
        });
        bounds_group.addLayer(layer_Jarakamandaribandarasejauh5km_2);
        map.addLayer(layer_Jarakamandaribandarasejauh5km_2);
        function pop_Jarakamandariterminalbussejauh4km_3(feature, layer) {
            layer.on({
                mouseout: function(e) {
                    for (i in e.target._eventParents) {
                        e.target._eventParents[i].resetStyle(e.target);
                    }
                },
                mouseover: highlightFeature,
            });
            var popupContent = '<table>\
                    <tr>\
                        <td colspan="2">' + (feature.properties['fid'] !== null ? Autolinker.link(String(feature.properties['fid'])) : '') + '</td>\
                    </tr>\
                    <tr>\
                        <td colspan="2">' + (feature.properties['REMARK'] !== null ? Autolinker.link(String(feature.properties['REMARK'])) : '') + '</td>\
                    </tr>\
                </table>';
            layer.bindPopup(popupContent, {maxHeight: 400});
        }

        function style_Jarakamandariterminalbussejauh4km_3_0() {
            return {
                pane: 'pane_Jarakamandariterminalbussejauh4km_3',
                opacity: 1,
                color: 'rgba(227,26,28,0.5)',
                dashArray: '10,5',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(253,13,57,0.5)',
            }
        }
        map.createPane('pane_Jarakamandariterminalbussejauh4km_3');
        map.getPane('pane_Jarakamandariterminalbussejauh4km_3').style.zIndex = 403;
        map.getPane('pane_Jarakamandariterminalbussejauh4km_3').style['mix-blend-mode'] = 'normal';
        var layer_Jarakamandariterminalbussejauh4km_3 = new L.geoJson(json_Jarakamandariterminalbussejauh4km_3, {
            attribution: '<a href=""></a>',
            pane: 'pane_Jarakamandariterminalbussejauh4km_3',
            onEachFeature: pop_Jarakamandariterminalbussejauh4km_3,
            style: style_Jarakamandariterminalbussejauh4km_3_0,
        });
        bounds_group.addLayer(layer_Jarakamandariterminalbussejauh4km_3);
        map.addLayer(layer_Jarakamandariterminalbussejauh4km_3);
        function pop_Jarakamandaristasiunsejauh3km_4(feature, layer) {
            layer.on({
                mouseout: function(e) {
                    for (i in e.target._eventParents) {
                        e.target._eventParents[i].resetStyle(e.target);
                    }
                },
                mouseover: highlightFeature,
            });
            var popupContent = '<table>\
                    <tr>\
                        <td colspan="2">' + (feature.properties['fid'] !== null ? Autolinker.link(String(feature.properties['fid'])) : '') + '</td>\
                    </tr>\
                    <tr>\
                        <td colspan="2">' + (feature.properties['railway'] !== null ? Autolinker.link(String(feature.properties['railway'])) : '') + '</td>\
                    </tr>\
                    <tr>\
                        <td colspan="2">' + (feature.properties['name'] !== null ? Autolinker.link(String(feature.properties['name'])) : '') + '</td>\
                    </tr>\
                </table>';
            layer.bindPopup(popupContent, {maxHeight: 400});
        }

        function style_Jarakamandaristasiunsejauh3km_4_0() {
            return {
                pane: 'pane_Jarakamandaristasiunsejauh3km_4',
                opacity: 1,
                color: 'rgba(227,26,28,0.5)',
                dashArray: '10,5',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(253,13,57,0.5)',
            }
        }
        map.createPane('pane_Jarakamandaristasiunsejauh3km_4');
        map.getPane('pane_Jarakamandaristasiunsejauh3km_4').style.zIndex = 404;
        map.getPane('pane_Jarakamandaristasiunsejauh3km_4').style['mix-blend-mode'] = 'normal';
        var layer_Jarakamandaristasiunsejauh3km_4 = new L.geoJson(json_Jarakamandaristasiunsejauh3km_4, {
            attribution: '<a href=""></a>',
            pane: 'pane_Jarakamandaristasiunsejauh3km_4',
            onEachFeature: pop_Jarakamandaristasiunsejauh3km_4,
            style: style_Jarakamandaristasiunsejauh3km_4_0,
        });
        bounds_group.addLayer(layer_Jarakamandaristasiunsejauh3km_4);
        map.addLayer(layer_Jarakamandaristasiunsejauh3km_4);
        function pop_Jarakamandaripelabuhansejauh2km_5(feature, layer) {
            layer.on({
                mouseout: function(e) {
                    for (i in e.target._eventParents) {
                        e.target._eventParents[i].resetStyle(e.target);
                    }
                },
                mouseover: highlightFeature,
            });
            var popupContent = '<table>\
                    <tr>\
                        <td colspan="2">' + (feature.properties['REMARK'] !== null ? Autolinker.link(String(feature.properties['REMARK'])) : '') + '</td>\
                    </tr>\
                </table>';
            layer.bindPopup(popupContent, {maxHeight: 400});
        }

        function style_Jarakamandaripelabuhansejauh2km_5_0() {
            return {
                pane: 'pane_Jarakamandaripelabuhansejauh2km_5',
                opacity: 1,
                color: 'rgba(227,26,28,0.5)',
                dashArray: '10,5',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(253,13,57,0.5)',
            }
        }
        map.createPane('pane_Jarakamandaripelabuhansejauh2km_5');
        map.getPane('pane_Jarakamandaripelabuhansejauh2km_5').style.zIndex = 405;
        map.getPane('pane_Jarakamandaripelabuhansejauh2km_5').style['mix-blend-mode'] = 'normal';
        var layer_Jarakamandaripelabuhansejauh2km_5 = new L.geoJson(json_Jarakamandaripelabuhansejauh2km_5, {
            attribution: '<a href=""></a>',
            pane: 'pane_Jarakamandaripelabuhansejauh2km_5',
            onEachFeature: pop_Jarakamandaripelabuhansejauh2km_5,
            style: style_Jarakamandaripelabuhansejauh2km_5_0,
        });
        bounds_group.addLayer(layer_Jarakamandaripelabuhansejauh2km_5);
        map.addLayer(layer_Jarakamandaripelabuhansejauh2km_5);
        function pop_JalurKA_6(feature, layer) {
            layer.on({
                mouseout: function(e) {
                    for (i in e.target._eventParents) {
                        e.target._eventParents[i].resetStyle(e.target);
                    }
                },
                mouseover: highlightFeature,
            });
            var popupContent = '<table>\
                    <tr>\
                        <td colspan="2">' + (feature.properties['railway'] !== null ? Autolinker.link(String(feature.properties['railway'])) : '') + '</td>\
                    </tr>\
                    <tr>\
                        <td colspan="2">' + (feature.properties['name'] !== null ? Autolinker.link(String(feature.properties['name'])) : '') + '</td>\
                    </tr>\
                </table>';
            layer.bindPopup(popupContent, {maxHeight: 400});
        }

        function style_JalurKA_6_0() {
            return {
                pane: 'pane_JalurKA_6',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'round',
                lineJoin: 'round',
                weight: 3.0,
                fillOpacity: 0,
            }
        }
        function style_JalurKA_6_1() {
            return {
                pane: 'pane_JalurKA_6',
                opacity: 1,
                color: 'rgba(255,255,255,1.0)',
                dashArray: '1,5',
                lineCap: 'round',
                lineJoin: 'round',
                weight: 2.0,
                fillOpacity: 0,
            }
        }
        map.createPane('pane_JalurKA_6');
        map.getPane('pane_JalurKA_6').style.zIndex = 406;
        map.getPane('pane_JalurKA_6').style['mix-blend-mode'] = 'normal';
        var layer_JalurKA_6 = new L.geoJson.multiStyle(json_JalurKA_6, {
            attribution: '<a href=""></a>',
            pane: 'pane_JalurKA_6',
            onEachFeature: pop_JalurKA_6,
            styles: [style_JalurKA_6_0,style_JalurKA_6_1,]
        });
        bounds_group.addLayer(layer_JalurKA_6);
        map.addLayer(layer_JalurKA_6);
        function pop_StasiunKA_7(feature, layer) {
            layer.on({
                mouseout: function(e) {
                    for (i in e.target._eventParents) {
                        e.target._eventParents[i].resetStyle(e.target);
                    }
                },
                mouseover: highlightFeature,
            });
            var popupContent = '<table>\
                    <tr>\
                        <td colspan="2">' + (feature.properties['railway'] !== null ? Autolinker.link(String(feature.properties['railway'])) : '') + '</td>\
                    </tr>\
                    <tr>\
                        <td colspan="2">' + (feature.properties['name'] !== null ? Autolinker.link(String(feature.properties['name'])) : '') + '</td>\
                    </tr>\
                </table>';
            layer.bindPopup(popupContent, {maxHeight: 400});
        }

        function style_StasiunKA_7_0() {
            return {
                pane: 'pane_StasiunKA_7',
        rotationAngle: 0.0,
        rotationOrigin: 'center center',
        icon: L.icon({
            iconUrl: '<?php echo base_url();?>assets/map/markers/transport_train_station2.svg',
            iconSize: [15.2, 15.2]
        }),
            }
        }
        map.createPane('pane_StasiunKA_7');
        map.getPane('pane_StasiunKA_7').style.zIndex = 407;
        map.getPane('pane_StasiunKA_7').style['mix-blend-mode'] = 'normal';
        var layer_StasiunKA_7 = new L.geoJson(json_StasiunKA_7, {
            attribution: '<a href=""></a>',
            pane: 'pane_StasiunKA_7',
            onEachFeature: pop_StasiunKA_7,
            pointToLayer: function (feature, latlng) {
                var context = {
                    feature: feature,
                    variables: {}
                };
                return L.marker(latlng, style_StasiunKA_7_0(feature));
            },
        });
        bounds_group.addLayer(layer_StasiunKA_7);
        map.addLayer(layer_StasiunKA_7);
        function pop_TerminalBus_8(feature, layer) {
            layer.on({
                mouseout: function(e) {
                    for (i in e.target._eventParents) {
                        e.target._eventParents[i].resetStyle(e.target);
                    }
                },
                mouseover: highlightFeature,
            });
            var popupContent = '<table>\
                    <tr>\
                        <td colspan="2">' + (feature.properties['REMARK'] !== null ? Autolinker.link(String(feature.properties['REMARK'])) : '') + '</td>\
                    </tr>\
                </table>';
            layer.bindPopup(popupContent, {maxHeight: 400});
        }

        function style_TerminalBus_8_0() {
            return {
                pane: 'pane_TerminalBus_8',
        rotationAngle: 0.0,
        rotationOrigin: 'center center',
        icon: L.icon({
            iconUrl: '<?php echo base_url();?>assets/map/markers/transport_bus_station.svg',
            iconSize: [11.4, 11.4]
        }),
            }
        }
        map.createPane('pane_TerminalBus_8');
        map.getPane('pane_TerminalBus_8').style.zIndex = 408;
        map.getPane('pane_TerminalBus_8').style['mix-blend-mode'] = 'normal';
        var layer_TerminalBus_8 = new L.geoJson(json_TerminalBus_8, {
            attribution: '<a href=""></a>',
            pane: 'pane_TerminalBus_8',
            onEachFeature: pop_TerminalBus_8,
            pointToLayer: function (feature, latlng) {
                var context = {
                    feature: feature,
                    variables: {}
                };
                return L.marker(latlng, style_TerminalBus_8_0(feature));
            },
        });
        bounds_group.addLayer(layer_TerminalBus_8);
        map.addLayer(layer_TerminalBus_8);
        function pop_Pelabuhan_9(feature, layer) {
            layer.on({
                mouseout: function(e) {
                    for (i in e.target._eventParents) {
                        e.target._eventParents[i].resetStyle(e.target);
                    }
                },
                mouseover: highlightFeature,
            });
            var popupContent = '<table>\
                    <tr>\
                        <td colspan="2">' + (feature.properties['REMARK'] !== null ? Autolinker.link(String(feature.properties['REMARK'])) : '') + '</td>\
                    </tr>\
                </table>';
            layer.bindPopup(popupContent, {maxHeight: 400});
        }

        function style_Pelabuhan_9_0() {
            return {
                pane: 'pane_Pelabuhan_9',
        rotationAngle: 0.0,
        rotationOrigin: 'center center',
        icon: L.icon({
            iconUrl: '<?php echo base_url();?>assets/map/markers/amenity=ferry_terminal.svg',
            iconSize: [19.0, 19.0]
        }),
            }
        }
        map.createPane('pane_Pelabuhan_9');
        map.getPane('pane_Pelabuhan_9').style.zIndex = 409;
        map.getPane('pane_Pelabuhan_9').style['mix-blend-mode'] = 'normal';
        var layer_Pelabuhan_9 = new L.geoJson(json_Pelabuhan_9, {
            attribution: '<a href=""></a>',
            pane: 'pane_Pelabuhan_9',
            onEachFeature: pop_Pelabuhan_9,
            pointToLayer: function (feature, latlng) {
                var context = {
                    feature: feature,
                    variables: {}
                };
                return L.marker(latlng, style_Pelabuhan_9_0(feature));
            },
        });
        bounds_group.addLayer(layer_Pelabuhan_9);
        map.addLayer(layer_Pelabuhan_9);
        function pop_Bandara_10(feature, layer) {
            layer.on({
                mouseout: function(e) {
                    for (i in e.target._eventParents) {
                        e.target._eventParents[i].resetStyle(e.target);
                    }
                },
                mouseover: highlightFeature,
            });
            var popupContent = '<table>\
                    <tr>\
                        <td colspan="2">' + (feature.properties['NMBDR'] !== null ? Autolinker.link(String(feature.properties['NMBDR'])) : '') + '</td>\
                    </tr>\
                    <tr>\
                        <td colspan="2">' + (feature.properties['NMKAB'] !== null ? Autolinker.link(String(feature.properties['NMKAB'])) : '') + '</td>\
                    </tr>\
                    <tr>\
                        <td colspan="2">' + (feature.properties['NMPROP'] !== null ? Autolinker.link(String(feature.properties['NMPROP'])) : '') + '</td>\
                    </tr>\
                </table>';
            layer.bindPopup(popupContent, {maxHeight: 400});
        }

        function style_Bandara_10_0() {
            return {
                pane: 'pane_Bandara_10',
        rotationAngle: 0.0,
        rotationOrigin: 'center center',
        icon: L.icon({
            iconUrl: '<?php echo base_url();?>assets/map/markers/transport_airport2.svg',
            iconSize: [19.0, 19.0]
        }),
            }
        }
        map.createPane('pane_Bandara_10');
        map.getPane('pane_Bandara_10').style.zIndex = 410;
        map.getPane('pane_Bandara_10').style['mix-blend-mode'] = 'normal';
        var layer_Bandara_10 = new L.geoJson(json_Bandara_10, {
            attribution: '<a href=""></a>',
            pane: 'pane_Bandara_10',
            onEachFeature: pop_Bandara_10,
            pointToLayer: function (feature, latlng) {
                var context = {
                    feature: feature,
                    variables: {}
                };
                return L.marker(latlng, style_Bandara_10_0(feature));
            },
        });
        bounds_group.addLayer(layer_Bandara_10);
        map.addLayer(layer_Bandara_10);
        var osmGeocoder = new L.Control.OSMGeocoder({
            collapsed: false,
            position: 'topright',
            text: 'Cari',
        });
        osmGeocoder.addTo(map);
        var baseMaps = {};
        L.control.layers(baseMaps,{'<img src="<?php echo base_url();?>/assets/map/legend/Bandara_10.png" /> Bandara': layer_Bandara_10,
        '<img src="<?php echo base_url();?>/assets/map/legend/Pelabuhan_9.png" /> Pelabuhan': layer_Pelabuhan_9,
        '<img src="<?php echo base_url();?>/assets/map/legend/TerminalBus_8.png" /> Terminal Bus': layer_TerminalBus_8,
        '<img src="<?php echo base_url();?>/assets/map/legend/StasiunKA_7.png" /> Stasiun KA': layer_StasiunKA_7,
        '<img src="<?php echo base_url();?>/assets/map/legend/JalurKA_6.png" /> Jalur KA': layer_JalurKA_6,
        '<img src="<?php echo base_url();?>/assets/map/legend/Jarakamandaripelabuhansejauh2km_5.png" /> Jarak aman dari pelabuhan sejauh 2 km': layer_Jarakamandaripelabuhansejauh2km_5,
        '<img src="<?php echo base_url();?>/assets/map/legend/Jarakamandaristasiunsejauh3km_4.png" /> Jarak aman dari stasiun sejauh 3 km': layer_Jarakamandaristasiunsejauh3km_4,
        '<img src="<?php echo base_url();?>/assets/map/legend/Jarakamandariterminalbussejauh4km_3.png" /> Jarak aman dari terminal bus sejauh 4 km': layer_Jarakamandariterminalbussejauh4km_3,
        '<img src="<?php echo base_url();?>/assets/map/legend/Jarakamandaribandarasejauh5km_2.png" /> Jarak aman dari bandara sejauh 5 km': layer_Jarakamandaribandarasejauh5km_2,
        'COVID-19<br /><table><tr><td style="text-align: center;"><img src="<?php echo base_url();?>/assets/map/legend/COVID19_1_00.png" /></td><td>0</td></tr><tr><td style="text-align: center;"><img src="<?php echo base_url();?>assets/map/legend/COVID19_1_11.png" /></td><td>1</td></tr><tr><td style="text-align: center;"><img src="<?php echo base_url();?>/assets/map/legend/COVID19_1_32.png" /></td><td>3</td></tr><tr><td style="text-align: center;"><img src="<?php echo base_url();?>/assets/map/legend/COVID19_1_43.png" /></td><td>4</td></tr><tr><td style="text-align: center;"><img src="<?php echo base_url();?>/assets/map/legend/COVID19_1_294.png" /></td><td>29</td></tr><tr><td style="text-align: center;"><img src="<?php echo base_url();?>/assets/map/legend/COVID19_1_5.png" /></td><td></td></tr></table>': layer_COVID19_1,"Kepadatan Penduduk": overlay_KepadatanPenduduk_0,}).addTo(map);
        setBounds();
        L.ImageOverlay.include({
            getBounds: function () {
                return this._bounds;
            }
        });
     </script>  

</body>

</html>
