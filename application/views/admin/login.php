<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>SIMERONA | Log in</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css'>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/login/css/style.css?v=1">
</head>

<body background="<?php echo base_url(); ?>assets/images/corona.png">
<div class="container main">
  <div class="row">
    <div class="col-md-6 col-md-offset-3 text-center title">
      <h1>LOGIN</h1>
      <h4 style="color: #ffffff">SIMERONA</h4>
      <div class="bar"></div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-md-offset-3 form">
      <h2></h2>
      <form action="<?php echo site_url('cont_login/login');?>" method="post">
        <input type="text" name="username" placeholder="username" required="required" /><br/>
        <input type="password" name="password" placeholder="password" required="required" />
        <button class="btn btn-default login" name="btn-login" type="submit">Login</button>
        <a class="btn btn-default signup" style="margin-left: 295px;" href="<?php echo site_url('cont_umum');?>">Home</a></span></h4>
      </form>
    </div>
  </div>
</div>
</body>

</html>
