<!DOCTYPE HTML>
<!--
	Spatial by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>SIMERONA</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/fe/css/main.css?v=1" />
	</head>
	<style>
		.pblack{
			color:black;
		}
		.button.special {
			background-color: #9c0000;
			color: #ffffff !important;
		}

			input[type="submit"].special:hover,
			input[type="reset"].special:hover,
			input[type="button"].special:hover,
			button.special:hover,
			.button.special:hover {
				background-color: #40b2f4;
			}
			#banner {
		padding: 16em 0 13em 0;
		background-attachment: fixed;
		background-image: url("<?php echo base_url('assets/images/corona.png') ?>");
		background-position: center top;
		background-size: cover;
		line-height: 1.75;
		text-align: center;
	}	
	</style>
	<body class="landing">

		<!-- Header -->
			<header id="header" class="alt">
				<h1><strong><a href="<?php echo site_url('cont_umum')?>">SIMERONA</a></strong></h1>
				<nav>
					<ul>
						<li><a href="<?php echo site_url('cont_login');?>">Login</a></li>
					</ul>
					
				</nav>
			</header>
			<!-- <a href="#menu" class="navPanelToggle"><span class="fa fa-bars"></span></a> -->
		<!-- Banner -->
			<section id="banner">
				<h2>Welcome To SIMERONA</h2>
					<h3 style="color:white;">Sistem Informasi Persebaran Virus Corona</h3>
			</section>

			<!-- One -->
				<section id="one" class="wrapper style1">
					<div class="container 75%">
						<div class="row 200%">
							<div class="6u 12u$(medium)">
								<header class="major">
									<h2>OVERVIEW</h2>
								</header>
							</div>
							<div class="6u$ 12u$(medium)">
								<p>SIMERONA adalah sistem informasi berbasis web yang dapat melihat peta zona bahaya Virus Corona berdasarkan moda transportasi serta demografi Orang Dalam Pengawasan (ODP), Pasien Dalam Pengawasan (PDP), Positif Corona, serta julah korban meninggal</p><br>
							</div>
						</div>
					</div>
				</section>

			<!-- Two -->
			<section id="two" class="wrapper style2 special">
					<div class="container">
						<header class="major">
							<h2>JENIS PETA</h2>
							<p>Pilih Jenis Peta</p>
						</header>
						<div class="row 150%">
							<div class="6u 12u$(xsmall)">
								<div class="image fit captioned">
									<img src="<?php echo base_url();?>/assets/images/transportasi.png" alt="" />
									<h3>peta zona bahaya transportasi</h3>
									<a href="<?php echo site_url('cont_umum/lihatpeta')?>" class="button special">LIHAT PETA</a>
								</div>
							</div>
							<div class="6u$ 12u$(xsmall)">
								<div class="image fit captioned">
								<img src="<?php echo base_url();?>/assets/images/demografi.png" alt="" />
									<h3>peta demografi status</h3>
									<a href="<?php echo site_url('cont_umum/lihatpetademografi')?>" class="button special">LIHAT PETA</a>	
								</div>
							</div>
							<div class="6u 12u$(xsmall)">
								<div class="image fit captioned">
									<img src="<?php echo base_url();?>/assets/images/lansia.png" alt="" />
									<h3>peta lanjut usia</h3>
									<a href="<?php echo site_url('cont_umum/lihatpetalansia')?>" class="button special">LIHAT PETA</a>
								</div>
							</div>
						</div>
						
					</div>
				</section>

			<!-- Three -->
				<section id="three" class="wrapper style1">
					<div class="container">
						<header class="major special">
							<h2>SUMMARY</h2>
							<p>Informasi Jumlah PDP, ODP, Positif Corona, OTG</p>
						</header>
						<div class="feature-grid">
							<div class="feature">
								<div class="image rounded"><img src="<?php echo base_url();?>assets/images/observation.png" alt="" /></div>
								<div class="content">
									<header>
										<h2><?php echo $sumodp?> Orang</h2>
										<p>ODP (Orang Dalam Pengawasan)</p>
									</header>
								</div>
							</div>
							<div class="feature">
								<div class="image rounded"><img src="<?php echo base_url();?>assets/images/patient.png" alt="" /></div>
								<div class="content">
									<header>
										<h2><?php echo $sumpdp ?> Orang</h2>
										<p>PDP (Pasien Dalam Pengawasan)</p>
									</header>
								</div>
							</div>
							<div class="feature">
								<div class="image rounded"><img src="<?php echo base_url();?>assets/images/covid.png" alt="" /></div>
								<div class="content">
									<header>
										<h2><?php echo $sumpositif ?> Orang</h2>
										<p>Positif</p>
									</header>
								</div>
							</div>
							<div class="feature">
								<div class="image rounded"><img src="<?php echo base_url();?>assets/images/what.png" alt="" /></div>
								<div class="content">
									<header>
										<h2><?php echo $sumotg?> Orang</h2>
										<p>OTG (Orang Tanpa Gejala)</p>
									</header>
								</div>
							</div>
						</div>
					</div>
				</section>



		<!-- Footer -->
			<footer id="footer" style="background:#9c0000;">
				<div class="container">
					<!-- <ul class="icons">
						<li style=><a href="https://www.facebook.com/" class="icon fa-facebook"></a></li>
						<li><a href="https://twitter.com/" class="icon fa-twitter"></a></li>
						<li><a href="https://www.instagram.com/mangunkarsa" class="icon fa-instagram"></a></li>
					</ul> -->
					<ul class="copyright">
						<li><br>&copy; 2020</li>
						<li>Build By:<br> SADEWA SOLUTION <br>Office: Masjid Sabilissalam, <br> Jl. Candi Mendut Selatan 30</a></li>
						<li>Build By:<br> GEOINFORMATICS RESEARCH CENTER <br> UNIVERSITY OF BRAWIJAYA <br>Office: Jl. Veteran 8</a></li>
						<li></a></li>
					</ul>
				</div>
			</footer>

		<!-- Scripts -->
			<script src="<?php echo base_url();?>assets/fe/js/jquery.min.js"></script>
			<script src="<?php echo base_url();?>assets/fe/js/skel.min.js"></script>
			<script src="<?php echo base_url();?>assets/fe/js/util.js"></script>
			<script src="<?php echo base_url();?>assets/fe/js/main.js"></script>

	</body>
</html>