<html lang="en">
    <head>   
    <title>SIMERONA | Peta Demografi Status</title>
	<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="initial-scale=1,user-scalable=no,maximum-scale=1,width=device-width">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">
        
		
   <link href="<?php echo base_url();?>assets/be/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
   <link href="<?php echo base_url();?>assets/css/bootstrap-switch.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url();?>assets/be/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>assets/be/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?php echo base_url();?>assets/be/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url();?>assets/be/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>assets/be/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/be/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url();?>assets/be/vendor/metisMenu/metisMenu.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-switch.min.js"></script>
    
     <!-- map style -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/map/css/leaflet.css">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/map/css/L.Control.Locate.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/map/css/qgis2web.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/map/css/Control.OSMGeocoder.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        

	 <!-- high charts -->
   <script src="https://code.highcharts.com/highcharts.js"></script>
   <script src="https://code.highcharts.com/modules/exporting.js"></script>

   <!-- map javascript -->
        <script src="<?php echo base_url();?>assets/map/js/qgis2web_expressions.js"></script>
        <script src="<?php echo base_url();?>assets/map/js/leaflet.js"></script>
        <script src="<?php echo base_url();?>assets/map/js/L.Control.Locate.min.js"></script>
        <script src="<?php echo base_url();?>assets/map/js/multi-style-layer.js"></script>
        <script src="<?php echo base_url();?>assets/map/js/leaflet.rotatedMarker.js"></script>
        <script src="<?php echo base_url();?>assets/map/js/leaflet.pattern.js"></script>
        <script src="<?php echo base_url();?>assets/map/js/leaflet-hash.js"></script>
        <script src="<?php echo base_url();?>assets/map/js/Autolinker.min.js"></script>
        <script src="<?php echo base_url();?>assets/map/js/rbush.min.js"></script>
        <script src="<?php echo base_url();?>assets/map/js/labelgun.min.js"></script>
        <script src="<?php echo base_url();?>assets/map/js/labels.js"></script>
        <script src="<?php echo base_url();?>assets/map/js/Control.OSMGeocoder.js"></script>
        <script src="<?php echo base_url();?>assets/map/data/Batas_Wilayah_Administrasi__Area__2.js"></script>
	 <style>
        #map {
            width: 100%;
            height: 541px;
        }		
		.footer {
		   left: 0;
		   bottom: 0;
		   width: 100%;
		   background-color: #9c0000;
		   color: white;
		   text-align: center;
		   margin-top:20px;
		}
		.table > thead > tr > th {
			vertical-align: middle;
		}
	</style>
	
 </head>
    <body>
	    
     <!-- Navigation -->
     <div>
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0; background-color: #9c0000;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <a class="navbar-brand" href="#" style="color: white;"><h3>Peta Demografi Status (ODP, PDP, Positif, OTG)</h3></a>
            </div>
    </div>       
            <!-- /.navbar-header -->
      <!--       <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                 -->
                <!-- /.dropdown-alerts -->
                <!-- /.dropdown -->
          <!--       <li>
                    <a href="<?php echo site_url('cont_umum/lihatpeta2');?>"><h4>Jalan Kabupaten</h4></a>
                </li>
                <li>
                    <a href="<?php echo site_url('cont_umum');?>"><h4>HOME</h4></a>
                </li>
                <li>
                    <a href="<?php echo site_url('cont_login');?>"><h4>LOGIN</h4></a>
                </li>
            </ul> -->
            <!-- /.navbar-top-links -->
            
           </nav> 
		 <div>  
		    <div>
            <div id="map" style="width:100%; height:83.4%; " class="leaflet html-widget html-widget-output">
        
                        <!-- <span style="float: right;margin-top:10px" > <input type="checkbox" name="checkboxpin2"  id="checkboxpin2"/> <span>         -->
                </div>
            </div>
			<div class="container">
		
        </div>
		
<!-- 			
       <div id="myModal" class="modal fade" role="dialog" class="col-md-12" >
  <div class="modal-dialog modal-lg"> -->

    <!-- Modal content-->
    <!-- <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 id="ket" class="modal-title"></h4>
      </div>
      <div class="modal-body">
		
        <button id="prov" type="button" class="btn btn-info">Tampilkan Info Propinsi</button>
        <button id="kab" type="button" class="btn btn-success">Tampilkan Info Kabupaten</button>
		</br>
        </br>
        <div id="hasil">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
 </div>
</div>

</div> -->
		<div class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-offset-3 col-md-3">
						<a href="<?php echo site_url('cont_umum');?>" style="color:white;"><h4>HOME</h4></a>
					</div>
					<div class="col-md-3">
						<a href="<?php echo site_url('cont_login');?>"style="color:white;"><h4>LOGIN</h4></a>
					</div>
				</div>
			</div>
        </div>
        <!-- untuk embed data administrasi -->
       
     <script>
     var map = L.map('map', {
            zoomControl:true, maxZoom:28, minZoom:1
        }).fitBounds([[-8.83275756506,110.808150163],[-6.38001452729,114.903681915]]);
        var hash = new L.Hash(map);
        map.attributionControl.addAttribution('<a href="https://github.com/tomchadwin/qgis2web" target="_blank">qgis2web</a>');
        L.control.locate().addTo(map);
        var bounds_group = new L.featureGroup([]);
        var basemap0 = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors,<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
            maxZoom: 28
        });
        basemap0.addTo(map);
        function setBounds() {
        }
        var overlay_GoogleRoad_0 = L.tileLayer('https://mt1.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
            opacity: 1.0
        });
        overlay_GoogleRoad_0.addTo(map);
        map.addLayer(overlay_GoogleRoad_0);
        function pop_Batas_Wilayah_Administrasi__Area__1(feature, layer) {
            var popupContent = '<table>\
                    <tr>\
                        <th scope="row">Kota</th>\
                        <td>' + (feature.properties['Kota'] !== null ? Autolinker.link(String(feature.properties['Kota'])) : '') + '</td>\
                    </tr>\
                </table>';
            layer.bindPopup(popupContent, {maxHeight: 400});
        }
        var legend = new L.Control({position: 'bottomleft'});
        legend.onAdd = function (map) {
          
          this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
          
          this.update();
          return this._div;
        };
        legend.update = function () {
          this._div.innerHTML = '<div class="card panel-right d-none d-md-block"><div class="card-body"><h3 class="card-title" style="color:black;">Hindari COVID-19</h3><p class="card-text text-justify">Peta ini memvisualisasikan Demografi<br> status masyarakat ODP, PDP, Positif, OTG<br> data diperoleh dari website resmi JATIM Tanggap Covid.<br>Peta ini tidak dapat digunakan sebagai landasan utama pencegahan,<br>hanya sebagai data pendukung.Diperlukan data<br> lain agar informasi lebih akurat.</p><footer class="blockquote-footer">Dikembangkan oleh : <cite title="Source Title">Fatwa Ramdani, Universitas Brawijaya</cite></footer><a href="https://www.unocha.org/" target="_blank">Sumber : Office For the Cordination of Humanitarian<br> Affairs</a></div></div>';
        };
        
        legend.addTo(map);
        function style_Batas_Wilayah_Administrasi__Area__1_0(feature) {
            switch(String(feature.properties['Kota'])) {
                case 'Bangkalan':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(255,245,240,1.0)',
            }
                    break;
                case 'Banyuwangi':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(255,241,234,1.0)',
            }
                    break;
                case 'Blitar':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(255,237,228,1.0)',
            }
                    break;
                case 'Bojonegoro':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(255,233,222,1.0)',
            }
                    break;
                case 'Bondowoso':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(255,228,216,1.0)',
            }
                    break;
                case 'Gresik':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(254,224,210,1.0)',
            }
                    break;
                case 'Jember':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(254,216,200,1.0)',
            }
                    break;
                case 'Jombang':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(254,209,190,1.0)',
            }
                    break;
                case 'Kediri':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(253,201,180,1.0)',
            }
                    break;
                case 'Kota Batu':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(253,194,170,1.0)',
            }
                    break;
                case 'Kota Blitar':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(252,186,160,1.0)',
            }
                    break;
                case 'Kota Kediri':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(252,178,150,1.0)',
            }
                    break;
                case 'Kota Madiun':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(252,170,141,1.0)',
            }
                    break;
                case 'Kota Malang':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(252,161,131,1.0)',
            }
                    break;
                case 'Kota Mojokerto':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(252,153,122,1.0)',
            }
                    break;
                case 'Kota Pasuruan':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(252,145,112,1.0)',
            }
                    break;
                case 'Kota Probolinggo':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(252,136,104,1.0)',
            }
                    break;
                case 'Kota Surabaya':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(252,128,96,1.0)',
            }
                    break;
                case 'Lamongan':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(252,120,88,1.0)',
            }
                    break;
                case 'Lumajang':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(252,112,80,1.0)',
            }
                    break;
                case 'Madiun':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(251,104,72,1.0)',
            }
                    break;
                case 'Magetan':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(248,94,66,1.0)',
            }
                    break;
                case 'Malang':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(246,85,60,1.0)',
            }
                    break;
                case 'Mojokerto':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(244,75,54,1.0)',
            }
                    break;
                case 'Nganjuk':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(241,65,48,1.0)',
            }
                    break;
                case 'Ngawi':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(237,57,43,1.0)',
            }
                    break;
                case 'Pacitan':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(230,49,40,1.0)',
            }
                    break;
                case 'Pamekasan':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(223,42,37,1.0)',
            }
                    break;
                case 'Pasuruan':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(215,35,34,1.0)',
            }
                    break;
                case 'Ponorogo':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(208,28,31,1.0)',
            }
                    break;
                case 'Probolinggo':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(200,23,28,1.0)',
            }
                    break;
                case 'Sampang':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(192,21,26,1.0)',
            }
                    break;
                case 'Sidoarjo':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(184,19,24,1.0)',
            }
                    break;
                case 'Situbondo':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(175,17,23,1.0)',
            }
                    break;
                case 'Sumenep':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(167,15,21,1.0)',
            }
                    break;
                case 'Trenggalek':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(152,11,19,1.0)',
            }
                    break;
                case 'Tuban':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(136,7,17,1.0)',
            }
                    break;
                case 'Tulungagung':
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(119,3,15,1.0)',
            }
                    break;
                default:
                    return {
                pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
                opacity: 1,
                color: 'rgba(0,0,0,1.0)',
                dashArray: '',
                lineCap: 'butt',
                lineJoin: 'miter',
                weight: 1.0, 
                fill: true,
                fillOpacity: 1,
                fillColor: 'rgba(103,0,13,1.0)',
            }
                    break;
            }
        }
        map.createPane('pane_Batas_Wilayah_Administrasi__Area__1');
        map.getPane('pane_Batas_Wilayah_Administrasi__Area__1').style.zIndex = 401;
        map.getPane('pane_Batas_Wilayah_Administrasi__Area__1').style['mix-blend-mode'] = 'normal';
        var layer_Batas_Wilayah_Administrasi__Area__1 = new L.geoJson(json_Batas_Wilayah_Administrasi__Area__1, {
            attribution: '<a href=""></a>',
            pane: 'pane_Batas_Wilayah_Administrasi__Area__1',
            onEachFeature: pop_Batas_Wilayah_Administrasi__Area__1,
            style: style_Batas_Wilayah_Administrasi__Area__1_0,
        });
        bounds_group.addLayer(layer_Batas_Wilayah_Administrasi__Area__1);
        map.addLayer(layer_Batas_Wilayah_Administrasi__Area__1);
        var osmGeocoder = new L.Control.OSMGeocoder({
            collapsed: false,
            position: 'topright',
            text: 'Cari',
        });
        osmGeocoder.addTo(map);
        setBounds();
        map.addControl(new L.Control.Search({
            layer: layer_Batas_Wilayah_Administrasi__Area__1,
            initial: false,
            hideMarkerOnCollapse: true,
            propertyName: 'Kota'}));
        </script>

    <!-- Morris Charts JavaScript -->
    <script src="<?php echo base_url();?>assets/be/vendor/raphael/raphael.min.js"></script>
    <script src="<?php echo base_url();?>assets/be/vendor/morrisjs/morris.min.js"></script>

    <script src="<?php echo base_url();?>assets/be/dist/js/sb-admin-2.js"></script>
    
    <!-- unused script 
    <script src="<?php echo base_url();?>assets/be/data/morris-data.js"></script>
    -->
    
    </body>
	</html>