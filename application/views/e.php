
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="initial-scale=1,user-scalable=no,maximum-scale=1,width=device-width">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/leaflet.css" />
        <link rel="stylesheet" type="<?php echo base_url();?>assets/text/css" href="css/qgis2web.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/label.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/MarkerCluster.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/MarkerCluster.Default.css" />
        <script src="<?php echo base_url();?>assets/js/leaflet.js"></script>
        <script src="<?php echo base_url();?>assets/js/OSMBuildings-Leaflet.js"></script>
        <script src="<?php echo base_url();?>assets/js/leaflet-hash.js"></script>
        <script src="<?php echo base_url();?>assets/js/label.js"></script>
        <script src="<?php echo base_url();?>assets/js/Autolinker.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/leaflet.markercluster.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <style>
        #map {
            width: 100%;
            height: 541px;
        }
		
        </style>
        <title></title>
    </head>
    <body>
		
			<div id="map">
			</div>
			
       <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
		
		
           <script src="http://localhost/simonara/assets/data/JLPROVJAWABALINTBNTT_7.js"></script>
        <script src="http://localhost/simonara/assets/data/JLPROVKALIMANTAN_6.js"></script>
        <script src="http://localhost/simonara/assets/data/JLPROVSULAWESIDANPAPUA_8.js"></script>
        <script src="http://localhost/simonara/assets/data/JLPROVSUMATERA_5.js"></script>
		
        <script src="http://localhost/simonara/assets/data/ADMINISTRASIKALIMANTAN_4.js"></script>
        <script src="http://localhost/simonara/assets/data/ADMINISTRASIPULAUBALIDANNUSATENGGARA_2.js"></script>
        <script src="http://localhost/simonara/assets/data/ADMINISTRASIPULAUJAWA_1.js"></script>
        <script src="http://localhost/simonara/assets/data/ADMINISTRASISULAWESIPAPUA_3.js"></script>
        <script src="http://localhost/simonara/assets/data/ADMINISTRASISUMATERA_0.js"></script>
       
		
	
		
		
		
		
        <script>
        L.ImageOverlay.include({
            getBounds: function () {
                return this._bounds;
            }
        });
        var map = L.map('map', {
            zoomControl:true, maxZoom:28, minZoom:1
        })
        var hash = new L.Hash(map);
        map.attributionControl.addAttribution('<a href="https://github.com/tomchadwin/qgis2web" target="_blank">qgis2web</a>');
        var feature_group = new L.featureGroup([]);
        var bounds_group = new L.featureGroup([]);
        var raster_group = new L.LayerGroup([]);
        var basemap0 = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors,<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
            maxZoom: 28
        });
        basemap0.addTo(map);
		var imageUrl = 'http://localhost/simonara/assets/data/IDN_msk_alt_18.png',
    imageBounds = [[5.462895560209557,94.8998203125], [5.462895560209557,141.1083984375],[6.001896,119.61914062499999],[-11.128086, 121.001196]];
	
L.imageOverlay(imageUrl, imageBounds).addTo(map);
        var initialOrder = new Array();
        var layerOrder = new Array();
        function stackLayers() {
            for (index = 0; index < initialOrder.length; index++) {
                map.removeLayer(initialOrder[index]);
                map.addLayer(initialOrder[index]);
            }
            if (bounds_group.getLayers().length) {
                map.fitBounds(bounds_group.getBounds());
            }
        }
        function restackLayers() {
            for (index = 0; index < layerOrder.length; index++) {
                layerOrder[index].bringToFront();
            }
        }
        layerControl = L.control.layers({},{},{collapsed:false});
		
		//jalan
		//stylejalan
        function styleJalan(feature) {
            return {
                weight: 1.04,
                color: '#7778d1',
                dashArray: '',
                lineCap: 'square',
                lineJoin: 'bevel',
                opacity: 1.0
            };
        }
        var jalan1 = new L.geoJson(json_JLPROVJAWABALINTBNTT_7, {
            //onEachFeature: pop_jalanv11,
            style: styleJalan
        });
        layerOrder[layerOrder.length] = jalan1;
        bounds_group.addLayer(jalan1);
        initialOrder[initialOrder.length] = jalan1;
        feature_group.addLayer(jalan1);
		//-jalan
		//jalan
       
        var jalan2 = new L.geoJson(json_JLPROVKALIMANTAN_6, {
            //onEachFeature: pop_jalanv11,
            style: styleJalan
        });
        layerOrder[layerOrder.length] = jalan2;
        bounds_group.addLayer(jalan2);
        initialOrder[initialOrder.length] = jalan2;
        feature_group.addLayer(jalan2);
		//-jalan
		
		//jalan
        
        var jalan3 = new L.geoJson(json_JLPROVSULAWESIDANPAPUA_8, {
            //onEachFeature: pop_jalanv11,
            style: styleJalan
        });
        layerOrder[layerOrder.length] = jalan3;
        bounds_group.addLayer(jalan3);
        initialOrder[initialOrder.length] = jalan3;
        feature_group.addLayer(jalan3);
		//-jalan
		
		//jalan
        var jalan4 = new L.geoJson(json_JLPROVSUMATERA_5, {
            //onEachFeature: pop_jalanv11,
            style: styleJalan
        });
        layerOrder[layerOrder.length] = jalan4;
        bounds_group.addLayer(jalan4);
        initialOrder[initialOrder.length] = jalan4;
        feature_group.addLayer(jalan4);
		//-jalan
       
        function styleADM(feature) {
            return {
                weight: 1.04,
                color: '#f44242',
                fillColor: '#ffffff',
                dashArray: '',
                lineCap: 'square',
                lineJoin: 'bevel',
                opacity: 1.0,
                fillOpacity: 0.0
            };
        }
        var adm1 = new L.geoJson(json_ADMINISTRASIPULAUJAWA_1, {
            style: styleADM
        });
        layerOrder[layerOrder.length] = adm1;
        bounds_group.addLayer(adm1);
        initialOrder[initialOrder.length] = adm1;
        feature_group.addLayer(adm1);
		
		 var adm2 = new L.geoJson(json_ADMINISTRASIKALIMANTAN_4, {
            style: styleADM
        });
        layerOrder[layerOrder.length] = adm2;
        bounds_group.addLayer(adm2);
        initialOrder[initialOrder.length] = adm2;
        feature_group.addLayer(adm2);
        
		var adm3 = new L.geoJson(json_ADMINISTRASISULAWESIPAPUA_3, {
            style: styleADM
        });
        layerOrder[layerOrder.length] = adm3;
        bounds_group.addLayer(adm3);
        initialOrder[initialOrder.length] = adm3;
        feature_group.addLayer(adm3);
        
		var adm4 = new L.geoJson(json_ADMINISTRASISUMATERA_0, {
            style: styleADM
        });
        layerOrder[layerOrder.length] = adm4;
        bounds_group.addLayer(adm4);
        initialOrder[initialOrder.length] = adm4;
        feature_group.addLayer(adm4);
        
		var adm5 = new L.geoJson(json_ADMINISTRASIPULAUBALIDANNUSATENGGARA_2, {
            style: styleADM
        });
        layerOrder[layerOrder.length] = adm5;
        bounds_group.addLayer(adm5);
        initialOrder[initialOrder.length] = adm5;
        feature_group.addLayer(adm5);
        
		
		
		
        raster_group.addTo(map);
        feature_group.addTo(map);
        stackLayers();
        map.on('overlayadd', restackLayers);
		
		
		
		adm1.on('click', function(e) {
			//alert('klik kiri adm1');
			$('#myModal').modal('show');
		});
		
		adm1.on('contextmenu', function(e) {
			alert('klik kanan adm1');
		});
		
		adm2.on('click', function(e) {
			alert('klik kiri adm2');
		});
		
		adm2.on('contextmenu', function(e) {
			alert('klik kanan adm2');
		});
		
		adm3.on('click', function(e) {
			alert('klik kiri adm3');
		});
		
		adm3.on('contextmenu', function(e) {
			alert('klik kanan adm3');
		});
		
		adm4.on('click', function(e) {
			alert('klik kiri adm4');
		});
		
		adm4.on('contextmenu', function(e) {
			alert('klik kanan adm4');
		});
		
		adm5.on('click', function(e) {
			alert('klik kiri adm5');
		});
		
		adm5.on('contextmenu', function(e) {
			alert('klik kanan adm5');
		});
		var markerTujuan=new Array();
		var id=0;
		map.on('click', function(e) {
			markerTujuan[id] = L.marker(L.latLng(e.latlng.lat,e.latlng.lng)).addTo(map);
			id++;
		});
		map.on('contextmenu', function(e) {
		var k;
		for(k=0;k<id;k++){
			map.removeLayer(markerTujuan[k]);
			}
			id=0;
		});
        </script>
		
		
		
    </body>
</html>
