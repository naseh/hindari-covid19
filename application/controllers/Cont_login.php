<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

    Class Cont_login extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->model('mod_user');
        }

        public function index()
        {
            $this->load->view('admin/login');
        }

        public function login()
        {
            $username = $this->input->post('username');
            $password = md5($this->input->post('password'));

            $cekuser = $this->mod_user->login($username,$password);

            if($this->session->userdata('username')!='')
            {
                redirect('cont_admin','refresh');
            }else
            {
                echo "<script>alert('Username atau password salah, login gagal')</script>";
                redirect('cont_login','refresh');
            }
        }

        public function logout()
        {
            $this->session->sess_destroy();
            echo "<script>alert('berhasil logout')</script>";
            redirect('cont_login','refresh');
        }
    }

?>