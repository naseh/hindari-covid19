<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

    class Cont_admin extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->model('mod_user');
            $this->load->model('mod_tabel');
            // $this->load->libary('session');
        }

        public function index()
		{
			// $this->load->view('admin/sidebar');
			$data['sumpdp'] = $this->mod_tabel->sumpdp();
			$data['sumodp'] = $this->mod_tabel->sumodp();
			$data['sumpositif'] = $this->mod_tabel->sumpositif();
			$data['sumotg'] = $this->mod_tabel->sumotg();
			$this->load->view('admin/index',$data);
		}
	}    
 ?>          