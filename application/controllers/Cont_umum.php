<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

    Class Cont_umum extends CI_Controller
    {
        public function __construct()
        {
          parent::__construct();
          $this->load->model('mod_tabel');
            
        }

        public function index()
        {
			$data['sumpdp'] = $this->mod_tabel->sumpdp();
			$data['sumodp'] = $this->mod_tabel->sumodp();
			$data['sumpositif'] = $this->mod_tabel->sumpositif();
			$data['sumotg'] = $this->mod_tabel->sumotg();
			$this->load->view('umum/index',$data);
        }

        public function uploadexcel()
        {
            $this->load->view('umum/upload');
        }

        public function prosesuploadexcel()
        {
            //dipindah ke cont_admin			
		}
		
		public function coba()
		{
			$this->load->view('umum/coba');
		}

        public function lihatpeta()
        {
            $this->load->view('umum/webgis');
        }
		
		public function lihatpetademografi()
        {
            $this->load->view('umum/webgis2');
		}
		
		public function lihatpetalansia()
        {
            $this->load->view('umum/webgis3');
        }
	}	
?>        